'''
@file       shares.py
@brief      
@details    
            \n *See source code here:*  <a href="link" 
            target="_blank" rel="noopener noreferrer">text</a> \n

@author     Ryan McLaughlin
@author     Matthew Frost
@date       Originally created on 05/24/21 \n Last modified on 05/24/21
'''

from task_share import Queue, Share
from micropython import const

dataToSave = 'x'

#####################################################################################
# define Queues

qLength = const(500)    
overWriteVar = True

time         = Queue('I', qLength, thread_protect = True, overwrite = overWriteVar, name = 'time')

if dataToSave == 'x':
    xData        = Queue('f', qLength, thread_protect = True, overwrite = overWriteVar, name = 'xData')
    xDotData     = Queue('f', qLength, thread_protect = True, overwrite = overWriteVar, name = 'xDotData')
    thYData      = Queue('f', qLength, thread_protect = True, overwrite = overWriteVar, name = 'thYData')
    thDotYData   = Queue('f', qLength, thread_protect = True, overwrite = overWriteVar, name = 'thDotYData')
    
else:     
    yData        = Queue('f', qLength, thread_protect = True, overwrite = overWriteVar, name = 'yData')
    yDotData     = Queue('f', qLength, thread_protect = True, overwrite = overWriteVar, name = 'yDotData')
    thXData      = Queue('f', qLength, thread_protect = True, overwrite = overWriteVar, name = 'thXData')
    thDotXData   = Queue('f', qLength, thread_protect = True, overwrite = overWriteVar, name = 'thDotXData')

# dutyXData    = Queue('f', qLength, thread_protect = True, overwrite = overWriteVar, name = 'dutyXData')
# dutyYData    = Queue('f', qLength, thread_protect = True, overwrite = overWriteVar, name = 'dutyYData')

cmd          = Queue('I', 10, thread_protect = True, overwrite = overWriteVar, name = 'commands')

#######################################################################################
# define Shared variables

# duty cycles, from controller to motor
dutyX    = Share('f', thread_protect = True, name ='dutyX')
dutyY    = Share('f', thread_protect = True, name ='dutyY')

# x and y position/velocity: from touch panel to controller
x        = Share('f', thread_protect = True, name ='x')
y        = Share('f', thread_protect = True, name ='y')

xDot     = Share('f', thread_protect = True, name ='xDot')
yDot     = Share('f', thread_protect = True, name ='yDot')

# platform angles (as read by the encoders)
thXE     = Share('f', thread_protect = True, name ='thXE')          
thYE     = Share('f', thread_protect = True, name ='thYE')

thDotXE  = Share('f', thread_protect = True, name ='thDotXE')
thDotYE  = Share('f', thread_protect = True, name ='thDotYE')

# platform angles (as read by the IMU) 
thXI     = Share('f', thread_protect = True, name ='thXI')
thYI     = Share('f', thread_protect = True, name ='thYI')

thDotXI  = Share('f', thread_protect = True, name ='thDotXI')
thDotYI  = Share('f', thread_protect = True, name ='thDotYI')

motFault = Share('I', thread_protect = True, name ='motorFault')

zeroEnc  = Share('I', thread_protect = True, name = 'zeroEncoders')

ballOn  = Share('I', thread_protect = True, name = 'ballOn')  
