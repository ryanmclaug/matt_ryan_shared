'''
@file       main.py
@brief      Runs the priority task scheduler.
@details    This program will automatically run on the Nucleo. This will add 
            tasks to the cotask.py list, and then run the priority-based
            scheduler. This will be the location where the user could tune the
            gain values or adjust the periods of the tasks.
            \n *See source code here:*  \n
            <a class="custom" href="https://bitbucket.org/ryanmclaug/matt_ryan_shared/src/master/405_Lab0xFF/Test%20Code/main.py" 
            target="_blank" rel="noopener noreferrer">Main Source Code</a>
@author     Ryan McLaughlin
@author     Matthew Frost
@date       Originally created on 05/21/21 \n Last modified on 06/03/21
'''

from micropython import alloc_emergency_exception_buf
import gc
import cotask, task_share
from motorTask import motorTask
from touchPanelTask import touchPanelTask
from encoderTask import encoderTask
from uiTask import uiTask
from ctrlTask import ctrlTask
from dataTask import dataTask
from pyb import UART
import pyb

# Allocate memory so that exceptions raised in interrupt service routines can
# generate useful diagnostic printouts
alloc_emergency_exception_buf (100)

if __name__ == "__main__":

    # periods for: [mottask, encTask, touchtask, ctrlTask, uiTask]
    ## @brief A list of periods that specify the frequency of tasks.
    periods =      [    11,       5,         5,       11,      30] 
    
    # kMatrix: [ position    ang_position   velocity    ang_velocity ]
    ## @brief  A list of K values for the x motor.
    kxMatrix = [2.4, 2.9, 10.0, 0.08] # Metal Ball
    ## @brief  A list of K values for the x motor.
    kyMatrix = [3.5, 2.9, 13.0, 0.08] # Metal Ball
    
    # Create objects of the tasks for our specific files
    ## @brief    Object for the Touch Panel Task
    tchPanelTask = touchPanelTask(debugFlag = False)
    ## @brief    Object for theEncoder Task
    encTask      = encoderTask(debugFlag = False)
    ## @brief    Object for the Motor Task
    motTask      = motorTask(mTaskFlag = False)
    ## @brief    Object for the Controller Task
    ctrlTask     = ctrlTask(kxMatrix, kyMatrix, debugFlag = False)
    ## @brief    Object for the User Interface Task
    uiTask       = uiTask(debugFlag = False)
    ## @brief    Object for the Data Collection Task
    dataColTask  = dataTask(debugFlag = False)
    
    # Create the tasks. If trace is enabled for any task, memory will be
    # allocated for state transition tracing, and the application will run out
    # of memory after a while and quit. Therefore, use tracing only for 
    # debugging and set trace to False when it's not needed
    ## @brief         Defining motorTask to be added to the Task List
    motorTaskObj      = cotask.Task (motTask.motorTaskFcn, name = 'motorTask', priority = 1, 
                                     period = periods[0], profile = True, trace = False)
    ## @brief         Defining encoderTask to be added to the Task List
    encoderTaskObj    = cotask.Task (encTask.encTaskFcn, name = 'encoderTask', priority = 1, 
                                     period = periods[1], profile = True, trace = False)
    ## @brief         Defining touchPanelTask to be added to the Task List
    touchPanelTaskObj = cotask.Task (tchPanelTask.tchPanelFcn, name = 'touchPanelTask', priority = 1, 
                                     period = periods[2], profile = True, trace = False)
    ## @brief         Defining controllerTask to be added to the Task List
    ctrlTaskObj       = cotask.Task (ctrlTask.ctrlTaskFcn, name = 'ctrlTask', priority = 1, 
                                     period = periods[3], profile = True, trace = False)
    ## @brief         Defining uiTask to be added to the Task List
    uiTaskObj         = cotask.Task (uiTask.uiTaskFcn, name = 'uiTask', priority = 1, 
                                     period = periods[4], profile = True, trace = False)

    # Add all tasks to the task list for the scheduler to run
    cotask.task_list.append(motorTaskObj)
    cotask.task_list.append(encoderTaskObj)
    cotask.task_list.append(touchPanelTaskObj)
    cotask.task_list.append(ctrlTaskObj)
    cotask.task_list.append(uiTaskObj)

    # Run the memory garbage collector to ensure memory is as defragmented as
    # possible before the real-time scheduler is started
    gc.collect ()
    
    # turn off REPL for the UART
    pyb.repl_uart(None) # disables repl on st-link usb port
    ## @brief Assign variable to UART port 2
    myuart = UART(2)
    
    print('\nTo operate the platform, start by levelling it, then press `b`.\n\n'
          'Place the ball onto the center of the platform.\n\n'
          'If the motor hits a fault, press the blue User button on the Nucleo'
          ' to clear the fault. Then, press `r` to reset, and `b` to balance'
          ' to restart once you have leveled the board.\n\n'
          'If at any time you want to stop the motors, press `s`. Follow the'
          ' same procedure as a fault to restart the program.\n\n')
    
    while True:
        try:
            cotask.task_list.pri_sched ()
        
        except KeyboardInterrupt:
            motTask.motorDriverObj.disable()
            print('Ctrl-C has been pressed, exiting program!\n')
            
            print('Please wait while data is being sent\n')
            dataColTask.dataTaskFcn()
            print('Data has finished saving\n')
            break

    # Print a table of task data and a table of shared information data
    print ('\n' + str (cotask.task_list) + '\n')
    print (task_share.show_all ())
    print ('\r\n')