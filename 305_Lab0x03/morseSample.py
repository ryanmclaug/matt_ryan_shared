'''
@file       filename.py
@brief      one line description here
@details    detailed description here
            
            \n *See source code here:* <insert link> \n
@image      html filename.jpg “Caption” width = xx% 
@author     Ryan McLaughlin
@date       Originally created on DD/MM/YY \n Last modified on DD/MM/YY
'''

CODE = {'A': '. -  ',     'B': '- . . .  ',   'C': '-.-.', 
        'D': '- . .  ',    'E': '.  ',      'F': '. . - .  ',
        'G': '- - .  ',    'H': '. . . .  ',   'I': '. .  ',
        'J': '. - - -  ',   'K': '- . -  ',    'L': '. - . .  ',
        'M': '- -  ',     'N': '- .  ',     'O': '- - -  ',
        'P': '. - - .  ',   'Q': '- - . -  ',   'R': '. - .  ',
        'S': '. . .  ',    'T': '-  ',      'U': '. . -  ',
        'V': '. . . -  ',   'W': '. - -  ',    'X': '- . . -  ',
        'Y': '- . - -  ',   'Z': '- - . .  ',
        }



def to_morse(s):
    return ' '.join(CODE.get(i.upper()) for i in s)

a = to_morse('RYAN')


def to_dots(s):
    a = ' '.join(CODE.get(i.upper()) for i in s)        # take string of letters, turn into morse code
    
    output = []
    for j in a:
        if j == '.':
            output.append('dot')
            # print('dot')
        elif j == '-':
            output.append('dash')
            # print('dash')
        else:
            output.append('space')
            # print('space')          
    return output
        
test = to_dots('RYAN')
print(test)