'''
@file       mcp9808.py
@brief      Class for MCP9808 temperature sensor
@details    This class contains various methods such as checking for a proper connection
            with the sensor, getting the current temperature in celsius or fahrenheit, and
            collecting data for a user-determined time interval/rate. Data is sent to and
            from the MCP sensor using I2C communication lines. More specifically, temperature
            measurements are sent through two bytes, with four of the bits representing
            various flags, and the remaining 12 bits containing temperature data.
            
            \n *See source code here:* <a href="https://bitbucket.org/ryanmclaug/matt_ryan_shared/src/master/405_Lab0x04/mcp9808.py" 
            target="_blank" rel="noopener noreferrer">MCP Class Source Code</a> \n

@author     Ryan McLaughlin
@author     Matt Frost
@date       Originally created on 05/06/21 \n Last modified on 05/11/21
'''

import pyb
from pyb import I2C
import utime
from array import array
import os
import sys

class mcp9808:
    
    ## @brief    Initial state
    #  @details  Connection to the sensor is checked and arrays are initialized
    S0_INIT             = 0
    ## @brief    Data collection state
    #  @details  Time, MCP, and Nucleo MCU data points are taken at regular time intervals
    S1_COLLECT_DATA     = 1
    ## @brief    CSV writing state
    #  @details  Data collected in state 2 is written line by line to a csv file
    S2_SAVE_DATA        = 2
    ## @brief    Exit state
    S3_DONE             = 3
    
    
    def __init__ (self, debugFlag = False):
        '''
        @brief  Constructor
        @param  debugFlag  user may choose `True` or `False` to control extra
                print statements used during code developement
        '''
        print('Creating a sensor object')
        
        ## @brief   Debugging flag for detailed analysis while running
        self.debugFlag = debugFlag
        
        ## @brief  Definition of I2C master
        self.i2c = I2C(1, I2C.MASTER)
        
        self.i2c.init(I2C.MASTER,baudrate = 100000)
        
        ## @brief  Address of slave (temperature sensor)
        self.addr = 0x18
        
        ## @brief  ADC for MCU temperature readings
        self.adc = pyb.ADCAll(12, 0x70000)
        
        ## @brief  Counter for writing to a csv
        self.sampleNum = 0
        
        ## @brief Contains the information for which state its in  
        self.state = 0
        
        ## @brief Current time, absolute, in milliseconds
        self.currentTime = utime.ticks_ms()
        
        ## @brief Time at which to take next data point
        self.nextTime = utime.ticks_ms()
        
        ## @brief Boolean variable used to establish refrence time value
        self.firstDataPoint = True
        

    def run(self):
        '''
        @brief    Main run method
        @details  Method run from a `main.py` script, which implements a finite state machine.
                  The path of this particular state machine is linear in nature, going from
                  the initial state, to data collection, to saving data, and ending with program
                  exit (with the potential to skip to program exit if certain conditions are met).
        '''
        if self.state == self.S0_INIT:
            
            ## @brief    Variable for starting program operation
            #  @details  In saving files to the Nucleo internal storage, the board must be reset
            #            (through unplugging and plugging the board back in) to access new data written,
            #            such as the csv file generated. However, this means the main.py script also runs
            #            automatically. To avoid data being erased on plugging the board back in, this input
            #            allows the user to decide if the program should be run.
            self.runMain = input('Would you like to run the program? This will delete the current csv file.' 
                                 'Enter "y" for yes, or "n" for no: ')
            if self.runMain == 'y':    
            
                self.check()        # sets self.proceed to True or False
                
                ## @brief    Boolean, set in the `check()` method
                #  @details  If the connection to the sensor is correct, `self.proceed` is set to True,
                #            and the program can continue
                if self.proceed == True:  # did check run correctly?
                    # clear any files (using append mode for csv writing)
                    self.clearFiles(['Lab0x04_data.csv'])    
                    
                    ## @brief  Data collection total time in minutes
                    self.sampleTime = int(input('Enter the total desired time for data collection session (in minutes): '))
                    
                    ## @brief  Data sampling rate in seconds (time between data points)
                    self.sampleRate = int(input('Enter the desired sample rate (in seconds): '))

                    ## @brief  Total number of samples based on user inputs
                    self.numSamples = int(self.sampleTime/(self.sampleRate/60) + 1)
                    print('number of samples = {:}'.format(self.numSamples))
                    
                    ## @brief  Time data array, of unsigned integers in milliseconds
                    self.timeArray   = array('I',[0]*self.numSamples)
                    ## @brief  MCP temperature data array, of signed floats in celsisus
                    self.mcpArray    = array('f',[0]*self.numSamples)
                    ## @brief  Nucleo MCU temperature data array, of signed floats in celsisus
                    self.nucleoArray = array('f',[0]*self.numSamples)
                    
                    self.transitionTo(self.S1_COLLECT_DATA)
            
                else:
                    print('Manufacturer ID was incorrect, exiting the program.')
                    self.transitionTo(self.S3_DONE)
            
            else:
                print('user has selected not to run the program, exiting now')
                self.transitionTo(self.S3_DONE)
                
        elif self.state == self.S1_COLLECT_DATA:
            
            self.currentTime = utime.ticks_ms()
            
            # has a period elapsed?
            if self.currentTime >= self.nextTime:
                
                if self.firstDataPoint == True:
                    self.firstDataPoint = False
                    
                    ## @brief  Time refrence point for calculated elapsed time
                    self.zeroTime = self.currentTime
                
                # update next time
                self.nextTime = self.currentTime + self.sampleRate*(10**3)
                
                ## @brief  Elapsed time since first data point was collected
                self.elapsedTime = utime.ticks_diff(self.currentTime,self.zeroTime)
                print('elapsed time is {:} milliseconds'.format(self.elapsedTime))
                
                # read from MCP
                
                ## @brief  Current MCP temperature in celsius
                self.currentTempMCP = self.celsius()
                ## @brief  Current MCP temperature in fahrenheit
                self.currentTempF = self.fahrenheit()
                print('Sensor temperature is {:} degrees celsius, '
                      '{:} degrees fahrenheit'.format(self.currentTempMCP,self.currentTempF))
                
                # read from Nucleo MCU
                
                self.adc.read_vref()
                ## @brief  Current Nucleo MCU temperature in celsius
                self.currentTempNucleo = self.adc.read_core_temp()
                print('Nucleo MCU temperature is {:} degrees celsius, '.format(self.currentTempNucleo))
                
                # place values in preallocated arrays
                self.timeArray  [self.sampleNum]   = self.elapsedTime
                self.mcpArray   [self.sampleNum]   = self.currentTempMCP
                self.nucleoArray[self.sampleNum]   = self.currentTempNucleo
                
                self.sampleNum += 1     # update iterator
                
            elif self.sampleNum >= self.numSamples :
                # data collection is complete, transition to writing to a csv
                self.sampleNum = 0
                self.transitionTo(self.S2_SAVE_DATA)
                
            
            
        elif self.state == self.S2_SAVE_DATA:
                
            if self.sampleNum <= (self.numSamples - 1):    # within limits of array size
                
                
                # open up a csv file, write with time and temp data from both nucleo and mcp
                _time        = self.timeArray  [self.sampleNum]
                _tempMCP     = self.mcpArray   [self.sampleNum]
                _tempNucleo  = self.nucleoArray[self.sampleNum]
                if self.debugFlag == True:
                    print('writing the following line to the csv file: ({:},{:},{:})'.format(_time,_tempMCP,_tempNucleo))
            
                with open('Lab0x04_data.csv',"a") as f:
                    f.write('{:},{:},{:}\n'.format(_time, _tempMCP, _tempNucleo))
                    
                self.sampleNum += 1
                
            else:
                self.transitionTo(self.S3_DONE)
        
    
        elif self.state == self.S3_DONE:
            sys.exit()
    
    
    
    
    def check(self):
        '''
        @brief  Method for checking whether the MCP9808 is configured correctly
        @details  To determine whether to continue with program operation, the system is checked
                  each time to ensure that the manufacturer ID register contains the correct information.
                  More specifically, reading from register pointer 0x05 of the I2C should give `b'\00T'`  
                  if a proper connection has been setup.
        '''
        _mID = self.i2c.mem_read(2,self.addr,6)
        if not _mID == b'\00T':
            if self.debugFlag == True:
                print('Invalid manufacturer ID: {:}'.format(str(_mID)))
            self.proceed = False
        else:
            if self.debugFlag == True:
                print('Manufacturer ID is correct, proceed with data collection.')
            self.proceed = True
        

    def celsius(self):
        '''
        @brief   Method for getting the current MCP9808 temperature in degrees Celsius
        @details This method utilizes reading from register pointer 0x05, which returns ambient
                 ambient temperature using 12 bits. The "upper byte" consists of a few checks 
                 such as critical temperature, as well as the sign bit. The "lower byte" consists
                 of the remainder of the data related to the temperature value, and a simple equation
                 using bit shifting allows for conversion from analog to digital temperature.
        @return  Ta Floating point number of ambient temperature (degrees celsius)
        '''
        dataByte = self.i2c.mem_read(2,self.addr,5)
        upperByte = dataByte[0]
        lowerByte = dataByte[1]
        
        # start by checking flag bits
        if (upperByte & 0x80) == 0x80:      # mask with 0b10000000 (MSB 1)
            # print('Ta > Tcrit')
            pass
        if (upperByte & 0x40) == 0x40:      # mask with 0b1000000  (MSB 2)
            # print('Ta > Tupper')
            pass
        if (upperByte & 0x20) == 0x20:      # mask with 0b100000   (MSB 3)
            # print('Ta < Tlower')
            pass
        
        # check sign bit to determine algorithm
        if (upperByte & 0x10) == 0x10:      # mask with 0b10000     (MSB 4)
            # Ta < 0      
            Ta = 256 - ( (upperByte & 0x0f)*16 + lowerByte/16 )
        else:
            # Ta > 0
            Ta = (upperByte & 0x0f)*16 + lowerByte/16 
        
        return Ta
        
    
    
    def fahrenheit(self):
        '''
        @brief   Method for getting the current MCP9808 temperature in degrees Fahrenheit
        @details See celsius method for details, as this method converts the value
                 returned by the celsisus method to fahrenheit
        @return  Ta Floating point number of ambient temperature (degrees fahrenheit)
        '''
        tC = self.celsius() 
        tF = 1.8*tC + 32
        return tF


    def clearFiles(self,filesList):
        '''
        @brief      Method for deleting files
        @details    Since the csv writer is set to append to a certain file, the file to be written to must be removed
                    each time the PC collects new data. If it was desired, this could be replaced by a method to rename
                    the next file to write to a new file each time. For this, the `os` module is used.
        @param      filesList is a list of strings containing the various files written each time the frontEnd is run 
        '''    
        _filesRemoved = 0
        
        for _fileName in filesList:
        
            if _fileName in os.listdir():       # check if file exists 
                os.remove(_fileName)
                _filesRemoved += 1
                
        print('{:} .csv/.png files have been removed'.format(_filesRemoved))


    def transitionTo(self, newState):      # state transition method
        '''
        @brief      Method for transitioning states
        @details    The method will reassign the `self.state` variable when
                    directed to transition.
        @param      newState Controls which state is the new state for the FSM.
        '''
        if self.debugFlag == True:
            print('UI: Transitioning from ' + 'S' + str(self.state) + '->' + 'S' + str(newState) )
        self.state = newState # now that transition has been declared, update state
            