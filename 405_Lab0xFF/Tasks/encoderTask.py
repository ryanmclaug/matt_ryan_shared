'''
@file       encoderTask.py
@brief      Interface with the encoder driver to find angle and angular speed.
@details    When called by the scheduler, this task gets the newest speed and
            angle from the encoderDriver.py and send that to shares.py. Also,
            during platform levelling, the encoders can be zeroed before the 
            controller begins.
            *See source code here:* 
            <a class="custom" href="https://bitbucket.org/ryanmclaug/matt_ryan_shared/src/master/405_Lab0xFF/Driver%20Classes/encoderTask.py" 
            target="_blank" rel="noopener noreferrer">Encoder Task Source Code</a>
@author     Matthew Frost
@author     Ryan McLaughlin
@date       Originally created on 05/21/21 \n Last modified on 06/03/21
'''
import pyb
from encoderDriver import encoderDriver
import micropython
from micropython import const
import shares
from utime import ticks_us, ticks_diff

# create an encoder object to pass in pins and timer
## @brief  An instance of the encoder driver on motor X
encY = encoderDriver(False, pin1 = pyb.Pin.cpu.C6, pin2 = pyb.Pin.cpu.C7, encTimer = 8) 
## @brief  An instance of the encoder driver on motor Y
encX = encoderDriver(False, pin1 = pyb.Pin.cpu.B6, pin2 = pyb.Pin.cpu.B7, encTimer = 4)  

class encoderTask:
    ''' 
     @brief    Task to call the encoder driver for current angles and velocities.
     @details  This task will be called from the priority scheduler to get the
               current angles and angular speeds of both encoders. This will be
               shared to the shares.py file for the controller task to read.
    '''
    ## @brief         The initial state to set up the task.
    S0_INIT           = const(0)
    ## @brief         State to zero the encoders at the start of the program.
    S1_ZERO_ENCODERS  = const(1)
    ## @brief         Main state to read from the encoders.
    S2_READ_ENCODERS  = const(2)   

    def __init__(self, debugFlag = True):
        ''' 
        @brief    Create an encoder task to find position and speeds.
        @details  This task will be called from the priority scheduler to get the
                  current angles and angular speeds of both encoders. This will be
                  shared to the shares.py file for the controller task to read.
        @param    debugFlag Enables or prevents certain print statements from
                  appearing.
       '''
        ## @brief        Debugging flag for detailed analysis while running
        self.debugFlag = debugFlag # used to view debugging print statements
        
    @micropython.native    
    def encTaskFcn(self):
        '''
        @brief    Main run function of encoder task
        @details  This function serves as a generator to run the finite state
                  machine of the encoder task. It has three states, and uses
                  an alpha-beta filter to determine position and velocity
                  of the platform.
        '''

        ## @brief    Contains the information for which state its in   
        state        = 0 # start in the sample data state
        ## @brief  The previous x encoder angle
        x0  = 0
        ## @brief  The previous x encoder angular velocity
        vx0 = 0
        ## @brief  The previous y encoder angle
        y0  = 0
        ## @brief  The previous y encoder angular velocity
        vy0 = 0

        while True:
            
            if state == self.S0_INIT:
                state = self.transitionTo(state,self.S1_ZERO_ENCODERS)
            
            elif state == self.S1_ZERO_ENCODERS:
                if shares.zeroEnc.get() & 1:
                    if self.debugFlag:
                        print('eTask: zeroing encoders')
                    encX.setPosition(0)
                    encY.setPosition(0)
                    ## @brief  The previous time
                    lastTime   = ticks_us()
                    state = self.transitionTo(state,self.S2_READ_ENCODERS)
            
            elif state == self.S2_READ_ENCODERS:
                
                ## @brief   The current time in microseconds
                currentTime = ticks_us()
                
                ## @brief  The current X angle
                xAngle     = encX.update()
                ## @brief  The current Y angle
                yAngle     = encY.update()

                # Find delta time since last computatation
                dt = ticks_diff(currentTime, lastTime)/1000000
                
                # get X Position and Velocity
                ## @brief  The current x angle
                x1         = x0 + 0.85*(xAngle - x0) + (vx0*dt)
                ## @brief  The current x angular velocity
                vx1        = vx0 + (0.005/dt)*(xAngle - x0)
                x0 = x1
                vx0 = vx1
                
                # get Y Position and Velocity
                ## @brief  The current y angle
                y1 = y0 + 0.85*(yAngle - y0) + (vy0*dt)
                ## @brief  The current y angular velocity
                vy1 = vy0 + (0.005/dt)*(yAngle - y0)
                y0 = y1
                vy0 = vy1

                # reset last time
                lastTime = currentTime
                
                # 6/11.5 = .5217
                shares.thXE.put(x1*.5217)
                shares.thDotXE.put(vx1*.5217)
                shares.thYE.put(y1*-.5217)
                shares.thDotYE.put(vy1*-.5217)
                
                if self.debugFlag:
                    print('eTask: cur angles:     {:}, {:} rad'.format(x1, y1))
                    print('eTask: cur ang. speed: {:}, {:} rad/s'.format(vx1, vy1))

            yield(state)
            
    @micropython.native        
    def transitionTo(self, state, newState):      # state transition method
        '''
        @brief      Method for transitioning states
        @details    The method will reassign the `state` variable when
                    directed to transition.
        @param      state The current state of the system.
        @param      newState The new desired state for the system.
        @return     The new state of the finite state machine.            
        '''
        if self.debugFlag:
            print('eTask: S' + str(state) + '->' + 'S' + str(newState) )

        return newState # now that transition has been declared, update state
