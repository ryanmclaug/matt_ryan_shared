## @file 405_Lab0xFF_Page.py
#  @brief Mainpage for the ME405 Lab Final
# 
## @page 405_lab0xFF 405 Lab 0xFF: Final Project
#
#  The final project for ME 405: Mechatronics, at California Polytechnic State
#  University, consisted of designing a closed-loop response for balancing a
#  ball on a platform. The platform pivots in two planes of motion, and has
#  two connections to two separate motors. These motors control a 4-bar linkage
#  system which controls the tilt angles of the platform. The ball rests on the
#  platform, which has a resistive touch panel for tracking the
#  position of the ball in both X and Y directions. \n
#  
#  @image      html board1.jpg " " width = 40%
#  @details    \n <CENTER> **Figure 1.** Balancing platform as seen from the motor side. </CENTER> \n
# 
#  This project spanned several assignments/weeks of the quarter. Thus, 
#  this project will be divided into several subsections to collectively 
#  represent the entirety of the work. See the list below to access the several
#  parts of the project that led up to the final project.\n
#
#  @section lab0xFF_HW  Modeling and Controller Design: Homework Assignments
#  Before any code could be written to interface with the hardware of the 
#  balancing platform system, the system was modeled and simulated over the course
#  of multiple homework assignments. The three sections below provide an overview
#  of work done prior to working with the physical system.
#  
#  - @subpage HW0x02
#  
#  - @subpage HW0x04
# 
#  - @subpage HW0x05
# 
#  @section  lab0xFF_lab Hardware and Code Implementation
#  Once the system had been fully analyzed from a theoretical standpoint, it was
#  time to interface with hardware and get the full integrated system working
#  together to balance the ball. The following pages describe code and general
#  theory of each hardware component, as well as system integration, controller
#  testing, and the final results.
# 
#  - @subpage touchPanelPage405
#
#  - @subpage motorPage405
#
#  - @subpage encoderPage405
#
#  - @subpage controllerPage405
#
#  - @subpage sysIntegration405
#
#  - @subpage testing405  
#
#  - @subpage finalResults405
# 
#  In total, this project provided a challenging, yet rewarding, exposure to
#  common hardware within the mechatronics world. The full process of
#  modeling, designing a controller, writing code, and tuning to achieve
#  a working system is invaluable experience for future projects. Furthermore,
#  learning to utilize task diagrams to plan out relations between various
#  tasks added a new skill to our mechatronics background.
#  
#  The final code can be seen at our online repository. Use the following link
#  in order to view the code:
#  
#  <a class="custom" href="https://bitbucket.org/ryanmclaug/matt_ryan_shared/src/master/405_Lab0xFF/" 
#  target="_blank" rel="noopener noreferrer">Lab0xFF Source Code</a> \n\n
#  
#  
##############################################################################  
## @page HW0x02 System Modeling: Kinetics and Kinematics
#  
#  @section HW0x02Intro System Definition 
# 
#  In the first homework assignment for this project included modeling the 
#  dynamics and kinematics of the ball and platform. Figure 2 shows the 
#  schematic of the platform as provided by the HW0x02 handout. \n\n
#  
#  @image      html 405_PlatformIsometric.png  " " width = 45%
#  @details    <CENTER> **Figure 2.** Platform schematic. </CENTER> \n\n
# 
# 
#  Additionally, visualizing the connection between the motor and platform is
#  critical to fully understand the test setup. Figure 3 shows the  
#  connection between the motor and platform. It consists of a motor arm AD and
#  push-rod DQ.\n\n
#  
#  @image      html 405_MotorArmSchematic.png " " width = 40%
#  @details    \n <CENTER> **Figure 3.** Motor to platform connection schematic. </CENTER> \n
# 
#  @section HW0x02Kinematics System Kinematics 
# 
#  After defining the geometry and such of the model, the kinematics of the  
#  ball were analyzed. This was done by defining the location of the ball in 
#  the global frame of reference, and then differentiating the equations to
#  find the velocity and acceleration of the ball. Along with finding the 
#  kinematics of the ball, a relationship between the motor speed and platform
#  angular speed was derived. \n\n
#  
#  To accomplish these, several simplifying assumptions were utilized, which 
#  will be summarized below:\n
#  
#  - For each motor, the linkage that connects the motor arm to the edge of the
#    platform remains solely vertical. This is under the assumption that the 
#    angles rotated by the motor are somewhat small, so the linkage itself won't
#    have too much deflection into the the horizontal/normal planes.\n
#  - The ball rolls without slipping. While this assumption is not necessarily
#    always true, it must be incorporated in order to complete the analysis.\n
#  - Since there are two motors, they are considered to be uncoupled. This 
#    simplifies the analysis by considering the platform as two independent  
#    beams rotating in two planes, rather than a platform moving in both
#    directions simultaneously.\n\n
# 
#  @section HW0x02Kinetics System Kinetics
# 
#  After deriving the kinematics, the kinetics were analyzed. First, the force
#  from the motor was translated into a resultant force and moment on the 
#  platform. This was done with a method named 'virtual work,' where the input
#  of work from the motor is imposed on the platform with no loss of energy.
#  This will simplify the kinetics in the future.\n\n
#  
#  To accomplish these, the main simplifying assumption is listed as such:\n
#  
#  - The linkages are massless. This is a fairly reasonable assumption, as the
#    mass of the motor arm and push-rod are much smaller than the inertia and 
#    mass of the platform. This assumption is necessary when considering the
#    virtual work analysis done to relate the motor torque to platform reaction
#    torque. \n\n
#  
#  
#  Now that the model has been simplified down, two separate FBDs and MADs were
#  created. The first system included the platform and ball as one. This 
#  produced a set of equations when moments were summed about the pivot point.
#  The second system was solely the ball, with moments being summed at the 
#  contact point. This eliminates the unnecessary friction and normal force
#  from the equations and generates another eqaution of motion. Once these are
#  derived, they are put into matrix form in terms of the linear acceleration
#  of the ball relative the platform and the angular acceleration of the
#  platform. These will be used in future assignments to allow for control of
#  the system.\n\n
#  
#  Continue to the next section here: \ref HW0x04
#  
##############################################################################  
## @page HW0x04 Linearization EOMs and Simulating Closed-Loop Control
#  
#  @section HW0x04Linearization Uncoupling and Linearizing EOMs
#  
#  For the Jupyter Notebook file used to develop the content of this section,
#  click here: <a class="custom" href="https://ryanmclaug.bitbucket.io/HW0x04_Jupyter.html" 
#  target="_blank" rel="noopener noreferrer">Jupyter Notebook</a> \n\n
#
#  In order to derive the closed-loop control, as well as simulate the expected
#  response of the system, the system of equations derived in \ref HW0x02 must
#  be decoupled and linearized in order to provide a representation of the sytem.
#  To start, the system was put into space-state form with a vector x defined
#  as holding the terms x (ball position), theta (platform rotation), and their
#  first derivatives.\n\n
#  
#  Also needed for linearization was an equilibrium point, which should be a 
#  stable 'resting' point of the system. In our case, the equilibrium point was
#  the point where the ball is above the center of gravity of the platform with
#  no velocity. Additionally, the angle and angular velocity of the platform
#  are zero. This eases the Jacobian linearization process immensely, as the
#  zeroth term of the Taylor series expansion is zero. Assuming that the ball
#  doesn't deviate far from the equilibrium point, it can also be assumed that
#  the higher order terms ("H.O.T.s") are negligible. This results in a 
#  Jacobian linearization process that can be followed in order to simulate and
#  model the set of equations to describe the motion of the platform and ball.
#  This consisted of getting an A and B matrix to represent the state of the 
#  system in the form of xdot = Ax + Bu, where u is the input vector, or motor
#  torque.\n
#  
#  @section HW0x04Simulation Simulating the Motion of the System
#  
#  Once the system has been put into the form of xdot = Ax + Bu,
#  the system can be solved with an ordinary differential solver or linear 
#  time invariant system solver. This was performed in Jupyter with the 'lti'
#  and 'lsim' functions within Python's sympy, numpy, and scipy.signal modules.
#  These were solved for two initial cases:\n\n
#  
#  - The ball starts above the center of gravity with no velocity, and no
#    tilt or angular velocity of the platform. There is no input torque from 
#    the motor.\n
#  - The ball starts 5cm from the center of gravity with no velocity. The 
#    platform has no tilt angle or initial velocity. Additionally, there is 
#    no input torque from the motor.\n\n
#  
#  As expected, the first case does not move the platform or ball, as it is
#  already at the equilibrium condition. However, for the second case, the
#  platform/ball system is not at equilibrium, and thus the position and 
#  velocity vectors are expected to be non-zero. This system was subsequently
#  tested in an open- and closed-looped control system.\n\n
#  
#  @subsection HW0x04OpenLoop Open Loop Response
#  
#  For the first test of the open loop system, the ball is modeled
#  as starting in the center of the platfrom (or in other words, the center of
#  mass of the ball is aligned with that of the platform). In this scenario,
#  no feedback is needed as the ball is in the steady state position. See Figure
#  4 below for plots of all four state variables.
# 
#  @image      html 405_HW0x04_OpenLoop1.png " " width = 70%
#  @details    \n <CENTER> **Figure 4.** Open-loop response for steady-state position. </CENTER> \n
#
#  However, if the ball starts in any other position, the system cannot return to 
#  equilibrium, as there is no feedback to impose an opposite force to level
#  the platform. Figure 5 below shows the system response for initial conditions
#  in which the ball is offset horizontally from the center of the platform by 5 cm.
#  
#  @image      html 405_HW0x04_OpenLoop2.png " " width = 70%
#  @details    \n <CENTER> **Figure 5.** Open-loop response for non-zero initial conditions. </CENTER> \n
# 
#  As is expected for this set of initial conditions and the open-loop configuration,
#  we can see that the system is unstable.
#  
#  @subsection HW0x04ClosedLoop Closed Loop Response
#  
#  When ran in the closed-loop configuration, the system can now use the 
#  feedback of the velocity and position to return to equilibrium. For now,
#  the closed-loop feedback was given as a function of the state variables,
#  but in the future will be tuned to optimize the response of the system.
#  Figure 6 displays the response of the system in the closed-loop control
#  system. This system behaves as expected, as the response is a second-order
#  response has a lot of overshoot as the platform jerks back and forth trying
#  to stabilize the ball. Eventually, the controller is able to stabilize the
#  system however, meaning the ball has returned back to equilibrium.
#  
#  
#  @image      html 405_HW0x04_ClosedLoop.png " " width = 70%
#  @details    \n <CENTER> **Figure 6.** Closed-loop response for same non-zero initial conditions. </CENTER> \n
#  
#  Continue to the next section here: \ref HW0x05
#  
##############################################################################  
## @page HW0x05 Full-State Feedback
#  
#  For the Jupyter Notebook file used to develop the content of this section,
#  click here: <a class="custom" href="https://matthewfrostcp.bitbucket.io/HW0x05_Jupyter.html" 
#  target="_blank" rel="noopener noreferrer">Jupyter Notebook</a> \n\n
#  
#  
#  @section HW0x05PoleSelection Deriving Pole Locations
#  In order to define the matrix [K1 K2 K3 K4] for a Full-State Feedback
#  controller, locations of the four poles were chosen in order to provide
#  a stable control loop with an expected settling time and percent overshoot.
#  For this particular assignment, a settling time of 2 seconds with a maximum
#  of 20% overshoot was deemed appropriate. From that, the damping ratio was 
#  calculated to be 0.45 with a natural frequency of 4.4 rad/s. With these
#  parameters, two of the poles could be determined. After selecting those
#  complex conjugate poles, the other two were chosen such that they were 
#  further to the left on the pole-zero plot as to interfere less with the
#  anticipated settling time and percent overshoot. The poles ended up as:
#  - -2 + 3.9j
#  - -2 - 3.9j
#  - -16
#  - -20
#  
#  A characteristic polynomial was created from this information. The 
#  coefficients of the characteristic polynomial were renamed as a0 thru a3 to 
#  signify the terms in front of the s^0 thru s^3 terms, respectively.\n\n
#  
#  Next, the A and B matrices calculated in \ref HW0x04 were solved in a Full-State
#  Feedback form of A-B*K to create an overall matrix, A_CL for "closed-loop."
#  K was in therms of K1, K2, K3, and K4, which are unknown controller gains
#  that need to be solved for in order to determine the four controller gains
#  needed for closed-loop control. A_CL was converted into a characteristic
#  polynomial after taking the determinant of s*I-A_CL. The coefficients of 
#  the characteristic polynomial were renamed as a0 thru a3 to signify the 
#  terms in front of the s^0 thru s^3 terms, similar to before. Lastly, the
#  a0 thru a3 terms solved with the expected characteristic polynomial were 
#  plugged into a set of equations to calculate K1, K2, K3, and K4, which 
#  should now hopefully simulate the sustem with a settling time near 2 seconds
#  and have an overshoot of 20%. \n\n
#  
#  @section HW0x05Results Results
#  As can be seen in Figure 7, the response of the ball is much quicker than
#  in \ref HW0x04. It has a settling time of nearly 2 seconds with under 20%
#  overshoot. The discrepancy between perfectly 2 seconds and 20% has to do 
#  with the fact that there are two more poles that are arbitrarily chosen
#  and affect those parameters.\n\n
#  
#  @image      html 405_HW0x05_ClosedLoop.png " " width = 70%
#  @details    \n <CENTER> **Figure 7.** Closed-loop response for new K matrix. </CENTER> \n
#  
#  To ensure that the calculations of K were valid, the poles and zeros of 
#  the A_CL matrix were determined. This is done as a sanity check to make sure
#  that the final poles line up with the chosen poles mentioned in
#  \ref HW0x05PoleSelection. Through the use of the eig() command and simplifying
#  tools within Jupyter, the final poles are as follows:
#  - -2 + 3.9j
#  - -2 - 3.9j
#  - -16
#  - -20
#  
#  Now that the model has been validated, the settling time and overshoot can
#  be modified in the future to gain a new K matrix (if desired), or at the
#  least, the current K matrix will act as a starting place to test on the 
#  physical balancing platform setup.
#
# Continue to the next section here: \ref touchPanelPage405
#  
##############################################################################  
## @page touchPanelPage405 Resistive Touch Panel: Locating the Ball
#
#  The first component neccesary for controlling the ball is the resistive touch panel.
#  The model used was a ER-TP080-1, 8 inch panel from East Rising Technology
#  (<a class="custom" href="http://buydisplay.com/download/manual/ER-TP080-1_Drawing.pdf" 
#  target="_blank" rel="noopener noreferrer">link</a> to datasheet). For the purposes
#  of this project, this was a great option based on its simplicity and relatively low price. 
#  To interface with the touch panel, there are three different measurments to be
#  made: x-position, y-position, and "z-position" (is the ball in contact with the
#  touch panel). To determine each reading, an analog to digital converter (ADC)
#  must be configured in a slightly different manner. For the two true positional
#  values, the board acts as a voltage divider, with the contact point of the ball
#  defining the division of voltage from the ADC. For measuring the z-component,
#  it is only neccesary to determine if any current is flowing.\n\n
#
#  Another important aspect of interfacing wtih the touch panel is correct calibration.
#  Values returned from a 12-bit ADC are from 0-4095, so to convert from ADC count to
#  (x,y) coordinates in millimeters, a calibration method and main program instruct the 
#  user to press down on a corner and the center of the board, from which
#  linear relations for x and y position can be determined. Additionally, as there
#  can be errors in readings if the ball is out of range of the readable area,
#  the user must select whether to keep a calibration run, or remove its data from the
#  final average calculations. This real-time error checking adds a neccesary human
#  element to the calibration process. (touchPanelCalibration_main.py, source code 
#  <a class="custom" href="https://bitbucket.org/ryanmclaug/matt_ryan_shared/src/master/405_Lab0xFF/Driver%20Classes/touchPanelCalibration_main.py" 
#  target="_blank" rel="noopener noreferrer">here</a>)
#
#  Lastly, the readings recieved from the ADC for the touch panel can be quite noisy,
#  giving rise to the need for filtering. Within the touch panel task (see Figure 8
#  below for the touch panel task state transition diagram), a few different types
#  of filtering are used. First, positional values must satsify an initial
#  threshold of "acceptable" (based on what the physical limits of readings should be).
#  For a more concrete example, the touch panel used for this project has a readable
#  area of approximately 176 mm by 107 mm, with the origin located at
#  its center. Therefore, a reading of x = 30 mm and y = 40 mm lies within the phyiscal
#  bounds, but a value of x = 90 mm and y = 60 mm is invalid (since both are more than 
#  half the board width/length respectively), and should be filtered out.
#
#  @image      html touchPanelTaskSTD.png " " width = 45%
#  @details    \n <CENTER> **Figure 8.** Touch panel task state transition diagram. </CENTER> \n
#
#  After this baseline filtering, an alpha-beta filter was used to obtain measurements
#  of both position and velocity with a relatively small amount of computational power
#  needed. Alpha-beta filters are a simplistic version of Kalman filters, and are often
#  used in control applications for data smoothing and estimation. For more information
#  on alpha-beta filters, see the following links:
#  
#  - <a class="custom" href="https://en.wikipedia.org/wiki/Alpha_beta_filter" 
#  target="_blank" rel="noopener noreferrer">Wikipedia Page</a>
#
#  - <a class="custom" href="https://towardsdatascience.com/alpha-beta-filter-21d3276cf35e" 
#  target="_blank" rel="noopener noreferrer">Article From towardsdatascience.com</a>
#
# 
#  For documentation of both the driver and task classes related to the touch panel, use the links
#  provided below:
#  - touchPanelDriver.py <a class="custom" href="https://bitbucket.org/ryanmclaug/matt_ryan_shared/src/master/405_Lab0xFF/Driver%20Classes/touchPanelDriver.py" 
#  target="_blank" rel="noopener noreferrer">Driver Source Code</a>
#  - touchPanelTask.py <a class="custom" href="https://bitbucket.org/ryanmclaug/matt_ryan_shared/src/master/405_Lab0xFF/Tasks/touchPanelTask.py" 
#  target="_blank" rel="noopener noreferrer">Task Source Code</a>
#
#
# Continue to the next section here: \ref motorPage405
#  
##############################################################################  
## @page motorPage405 DC Motors: Moving the Platform
#
#  The second hardware componenet to interface with was the DC motors/motor driver.
#  The combination of motors and motor driver was the actuator of the control system,
#  as the controller's final goal was to determine an appropriate duty cycle for each
#  motor (at a fast rate) to balance the ball. The motor driver used was a DRV8847
#  Dual H-Bridge Motor Driver from Texas Instruments 
#  (link to <a class="custom" href="https://www.ti.com/document-viewer/DRV8847/datasheet/revision-history-gdss3571158#GDSS3571158" 
#  target="_blank" rel="noopener noreferrer">Interactive Datasheet</a>), and the 
#  motors themselves were Maxon DCX 22 S DC Motors 
#  (<a class="custom" href="https://www.maxongroup.com/maxon/view/product/motor/dcmotor/DCX/DCX22/DCX22S01GBKL468" 
#  target="_blank" rel="noopener noreferrer">Product Page</a>).
#
#  @image      html maxonMotor.png " " width = 30%
#  @details    \n <CENTER> **Figure 9.** Maxon DCX 22S DC motor (Source: see above link). </CENTER> \n
#
#  In terms of code implementation, the major difference between motor control
#  employed in ME 305 and ME 405 was the seperation of motor driver from
#  individual motor channels. This divide is desirable because certain methods can
#  be written that affect both motors. For example, the nSleep pin that is used to
#  disable/enable the motor driver applies for both motors. Similarly, the nFault
#  pin can be triggered by either motor, and disables both automatically. The last,
#  more organizational method of the parent motor driver is a method to create
#  motor channel objects from the motor driver chanel class. By giving the parent driver
#  the ability to create motor objects, the driver can store copies of these motors, and
#  disable or enable both at the same time.
#
#  In terms of the motor driver channel class, the only method needed is one to configure
#  PWM pins for the correct direction and effort. This method, `setLevel()`, and the methods
#  of the parent motor driver can be found in the following documentation pages.
#
#  - motorDriver.py <a class="custom" href="https://bitbucket.org/ryanmclaug/matt_ryan_shared/src/master/405_Lab0xFF/Driver%20Classes/motorDriver.py" 
#  target="_blank" rel="noopener noreferrer">Parent Driver Source Code</a>
#  - motorDriverChannel.py <a class="custom" href="https://bitbucket.org/ryanmclaug/matt_ryan_shared/src/master/405_Lab0xFF/Driver%20Classes/motorDriverChannel.py" 
#  target="_blank" rel="noopener noreferrer">Driver Channel Source Code</a>
#
#  Lastly, the motor task had two main states: moving the motors, and a latch state.
#  In the moving motors state, desired duty cycles are recieved from the controller
#  and are saved to arrays for later analysis. In the case of a fault, the task
#  moves to the latch state, in which a user must manully press the blue user
#  button on the Nucleo board to clear the fault. The state transition diagram for
#  the motor task can be seen below in Figure 10.
#
#  @image      html motorTaskSTD.png " " width = 40%
#  @details    \n <CENTER> **Figure 10.** Motor task state transition diagram. </CENTER> \n
#
#  For documentation of the motor task, see the below links:
#  - motorTask.py <a class="custom" href="https://bitbucket.org/ryanmclaug/matt_ryan_shared/src/master/405_Lab0xFF/Tasks/motorTask.py" 
#  target="_blank" rel="noopener noreferrer">Motor Task Source Code</a>
#  
#
# Continue to the next section here: \ref encoderPage405
#  
##############################################################################  
## @page encoderPage405 Quadrature Encoders: Measuring Motor Position
#
#  To measure the position of each motor (and subsequently the approximate angles of the board)
#  quadrature encoders were used. The model selected for this project was an E4T Miniature
#  Optical Encoder (see Figure 11). As with all components for this project, low cost with sufficient
#  performance was the goal (link to
#  <a class="custom" href="https://cdn.usdigital.com/assets/datasheets/E4T_datasheet.pdf?k=637509874415141733" 
#  target="_blank" rel="noopener noreferrer">E4T Datasheet</a>).
#
#  @image      html e4tEncoder.png " " width = 22%
#  @details    \n <CENTER> **Figure 11.** E4T miniature optical encoder \n(Source: <a class="custom" 
#  href="https://www.usdigital.com/products/encoders/incremental/kit/E4T" target="_blank"
#   rel="noopener noreferrer">https://www.usdigital.com/products/encoders/incremental/kit/E4T)</a>
#   </CENTER> \n
#
#  To interface with encoders, there were two main steps to take. First, each needed
#  to be correctly configured. For configuration, a timer object must be instantiated
#  with proper prescaler (how many ticks per count) and period (how many counts to 
#  overflow), which for the purposes of this project were 1 and 0xffff (65535)
#  respectively. Secondly, the encoders do not give any warning of overflow/underflow,
#  so we must algorithmically account for this. The algorithm used determined a change
#  in count each time the encoders are updated. If this change in tick count is between
#  -1/2 to +1/2 of the timer period, no correction is needed. However for values outside
#  of this range, the period must be added or subtracted (for underflow and overflow
#  respectively). After neccesary corrections are made, the current delta value is added
#  to the previous position to obtain the current position. 
# 
#  As with all components of this project, a driver class and task were both written 
#  for the encoders. The main method of the driver, `update()`, was responsible for the
#  algorithm discussed above. Additionally, a `setPosition()` method was utilized for
#  resetting the encoders upon request from the user, such as in the case of zeroing
#  the board before balancing the ball. A method `getSpeed()` for speed measurement
#  was originally written for the purposes of ME 305, but was not used for this project
#  with the use of filtering in the task. On the task side of things, the alpha-beta 
#  filter approach introduced in \ref touchPanelPage405 was again used to estimate
#  position and velocity. These values could then be stored in a shared variable, 
#  and accesed by the controller as needed. For the corresponding state transition
#  diagram see Figure 12 below.
#
#  @image      html encoderTaskSTD.png " " width = 35%
#  @details    \n <CENTER> **Figure 12.** Encoder task state transition diagram. </CENTER> \n
#
#  For detailed file documentation, see the links below:
#  - encoderDriver.py <a class="custom" href="https://bitbucket.org/ryanmclaug/matt_ryan_shared/src/master/405_Lab0xFF/Driver%20Classes/encoderDriver.py" 
#  target="_blank" rel="noopener noreferrer">Driver Source Code</a>
#  - encoderTask.py <a class="custom" href="https://bitbucket.org/ryanmclaug/matt_ryan_shared/src/master/405_Lab0xFF/Tasks/encoderTask.py" 
#  target="_blank" rel="noopener noreferrer">Task Source Code</a>
#
# Continue to the next section here: \ref controllerPage405
# 
################################################################################
## @page controllerPage405 User Interface (UI) and Controller
#
#  With the touch panel, encoders, and motors all setup properly, the user interface
#  and controller were the last two main pieces of the full system. 
#
#  @section uiTask405 User Inteface (UI)
#  Before discussing the controller, a brief explanation on how the user interface
#  works is neccesary. Using the `USB_VCP` module from the micropython library,
#  key presses can be read and used as a direct link between the user and the Nucleo
#  microcontroller (VCP stands for virtual comm port). The key methods used were
#  `USB_VCP.any()`, which determines if a key has been pressed and data is ready
#  for reading, and `USB_VCP.read()`, which reads byte data from the virtual
#  comm port object. The UI task does not utilize a finite state machine, but instead
#  checks each run if there is data to be read. If data is available, it is read
#  as an ASCII character code. Finally, if the character read matches one of the
#  available commands, it is sent to the controller to be processed. With this checking
#  system, the controller only needs to handle correct commands from the user. 
#  The three available commands are:
#       1. b, platform is balanced and ready for the ball
#       2. s, stop the motors (the emergency stop)
#       3. r, reset the platform for a new trial
#
#  Each command can only be used in certain controller states, but these are the
#  three main controls (along with the blue user button for clearing faults) 
#  for running the system from a human perspective. However most of the system
#  control is run autonomously, as discussed in the next section.
# 
#  For documentation of the UI task, see the following links below:
#  - uiTask.py <a class="custom" href="https://bitbucket.org/ryanmclaug/matt_ryan_shared/src/master/405_Lab0xFF/Tasks/uiTask.py" 
#  target="_blank" rel="noopener noreferrer">Source Code</a>
#
# 
#  @section controller Controller
#  The controller acted as the main hub for information. Feedback in the form of
#  encoder and touch panel readings (position and velocity measurement), as well
#  as commands from the user interface are read by the controller, and from
#  this stream of signals the controller must determine what action to take.
#  For full discussion of the modeling of the controller, see \ref HW0x05. In short,
#  state space control with full-state feedback allows the controller to use
#  multiple signals to determine the needed duty cycle for the motors. 
#
#  @image      html ctrlTaskSTD.png " " width = 40%
#  @details    \n <CENTER> **Figure 13.** Controller task state transition diagram. </CENTER> \n
#
#  As seen in Figure 13 above, the finite state machine of the controller has 
#  four main states. Initially, the controller must wait for the user to press
#  either "b", telling the controller that the platorm is level and the ball
#  can now be placed on the platform, or the user can press "s" to stop the motors.
#  Originally, the plan was to use an Inertial Measurement Unit (IMU) to automate
#  the balancing of the platform, however time constraints limited us to manually
#  leveling the platform. Once the controller recieves a signal from the touch
#  panel task that the ball is on the platform, a transition occurs to the 
#  most important state, S2_BALANCE_BALL. In state 2, the controller reads
#  from both encoders and the touch panel for feedback, sends updated duty cycles
#  to the motors, and adds the current feedback data to queues for data collection
#  purposes. From state 2, either a fault can occur or the user can choose to press
#  "s", which both switch the controller to S3_STOP_MOTORS. From state 3, the board
#  can then be reset to try balancing the ball again.
#  
#  For documentation of the controller task, use the links provided below:
#  - ctrlTask.py <a class="custom" href="https://bitbucket.org/ryanmclaug/matt_ryan_shared/src/master/405_Lab0xFF/Tasks/ctrlTask.py" 
#  target="_blank" rel="noopener noreferrer">Source Code</a>
#
#
# Continue to the next section here: \ref sysIntegration405
#  
##############################################################################  
## @page sysIntegration405 System Integration: Task Scheduling, Data Sharing, and Data Collection
#  Once all the tasks and drivers were written, combining each function into a
#  complete software program was the next step. This included using a scheduler
#  to run the tasks in a specific order, implementing a method for sharing data
#  and variables between tasks, and saving data for post-processing and plotting.\n\n
#  
#  To facilitate the culmination of tasks and shared variables, a task diagram 
#  was created in order to organize the program flow, as seen in Figure 14.
# 
#  @image      html TaskDiagram.png " " width = 60%
#  @details    \n <CENTER> **Figure 14.** Task diagram for shared variables, frequency of tasks, and priorities. </CENTER> \n 
#  
#  The following sections will explore each topic in greater depth.
#  
#  
#  @section scheduler Scheduler: Timing Management
#  To facilitate multitasking between scripts, a priority-driven scheduler was
#  utilized. This scheduler was provided to the students from the instructors, 
#  but allowed for tasks to be added task list, where they were run at specified
#  intervals to properly implement the controller. The scheduler ran tasks according
#  to their priority: a higher priority meant that if two tasks were trying to 
#  run at the same time, the scheduler would run the higher priority task first.
#  For this project, all of the tasks and controllers had an equal priority of
#  1, and the original data collection task had a priority of 0. When specifying
#  the frequency to run the tasks, it was important to keep in mind how long the
#  needed to run for, as trying to run the tasks faster than they can complete 
#  is not practical and leads to slowing the program down or running future tasks
#  late. \n\n
#  
#  Since timing was crucial, each task was optimized to run in the least amount 
#  of time as possible. This required cutting down code, eliminating print
#  statements, reducing the number of stored variables, and many more techniques
#  to save time and memory.\n\n
#  
#  For detailed file documentation, see the link below:
#  - cotask.py <a class="custom" href="https://bitbucket.org/ryanmclaug/matt_ryan_shared/src/master/405_Lab0xFF/Scheduler/cotask.py" 
#  target="_blank" rel="noopener noreferrer">Cooperative Scheduler Code</a>
#  
#  
#  @section shareFile Sharing Data
#  Throughout the program, several variables had to be shared between tasks in
#  order to implement the full-state feedback control algorithm. These included
#  the position/velocity of the ball as well as the platform angle/angular 
#  velocity. From these, the duty cycle was computed and shared to the motor 
#  task to drive the motors. \n\n
#  
#  To facilitate the shared variables, a file was provided in order to host the
#  shared variables and shared queues. For the shared variables, any task with
#  access to the task-sharing file could @c put() measured values to the variable,
#  and then other tasks can @c get() to read those values. Similar structures
#  were used for the queues, where a first-in-first-out system was used.
#  The queues consisted of duplicates of the data, which would be saved in the
#  largest queue possible to allow for plotting the measured values after the
#  script was run.\n\n
#  
#  To host all the shared variable declarations in one location, shares.py was
#  created.\n\n
# 
#  For detailed file documentation, see the links below:
#  - task_share.py <a class="custom" href="https://bitbucket.org/ryanmclaug/matt_ryan_shared/src/master/405_Lab0xFF/Scheduler/task_share.py" 
#  target="_blank" rel="noopener noreferrer">Task Share Code</a>
#  - shares.py <a class="custom" href="https://bitbucket.org/ryanmclaug/matt_ryan_shared/src/master/405_Lab0xFF/Scheduler/shares.py" 
#  target="_blank" rel="noopener noreferrer">Shares Code</a>
#  
#  
#  @section dataCollection Sending Data from the Nucleo
#  Originally, a data collection task was ran in the scheduler with a low
#  priority, however, it was found that it never had enough time to get data
#  from the shared queues and send it through serial to the front end. Similarly,
#  it didn't have enough time to 'batch' up shared data to send in one go at
#  the end of the program.\n\n
#  
#  Because of these timing issues, the data collection was reformatted to 
#  continuously save data to shares.py, and then when the user stops the program
#  with Ctrl-C, the data is sent through serial. This eliminates the worry for
#  speeding up the program, as speed is less of a consideration once the program
#  finishes. Due to memory issues on the board, only a few variables could be
#  storing data each run. For a given run, the position/velocity of the ball and
#  angle/angular velocity of the platform in one direction, as well as the time,
#  could be saved and plotted in the front end.\n\n
#  
#  For detailed file documentation, see the link below:
#  - dataTask.py <a class="custom" href="https://bitbucket.org/ryanmclaug/matt_ryan_shared/src/master/405_Lab0xFF/Tasks/dataTask.py" 
#  target="_blank" rel="noopener noreferrer">Data Collection Task Code</a>
#  \n\n
#  @section pcFrontend  PC Frontend: Facilitating Data Collection and Plotting
#  The PC Frontend facilitated the collection of data streamed through the
#  serial port, writing to a CSV, and then plotting. An automated plotting
#  function was created within the front end to read the columns of the CSV
#  and display 4 subplots. These were the ball's position/velocity and platform's
#  angle/angular velocity for a given motor. This file was then saved as a png
#  for inspection and analysis. \n\n
#  
#  For detailed file documentation, see the link below:
#  - PCFrontEnd.py <a class="custom" href="https://bitbucket.org/ryanmclaug/matt_ryan_shared/src/master/405_Lab0xFF/Tasks/PCFrontEnd.py" 
#  target="_blank" rel="noopener noreferrer">PC Front End Code</a>
#
#
# Continue to the next section here: \ref testing405
#  
##############################################################################  
## @page testing405 Testing and Tuning: Balancing the Ball
#  Once all the files were written, 20-25 hours of tuning was required in order
#  to successfully balance the ball on the platform. This page will discuss the
#  major aspects of the program that needed to be tuned, as well as any issues 
#  that were encountered in the system.\n\n
#  
#  @section Kmatrix Tuning the K Matrix
#  The majority of tuning revolved around modifying the system gains of the
#  full-state feedback controller. These gains were defined in the main script
#  from which all task objects were instantiated. For detailed documentation of
#  the main script, see main.py (<a class="custom" 
#  href="https://bitbucket.org/ryanmclaug/matt_ryan_shared/src/master/405_Lab0xFF/Test%20Code/main.py"
#  target="_blank" rel="noopener noreferrer">Main Script Source Code</a>).
#  While these were derived in HW0x05, that model
#  was based on a very idealized system, so it was bound to not work perfectly.
#  This tuning took the most time, as there were 8 values that could be modified,
#  and had to be tested after each modification.\n\n
#  
#  When tuning the K matrix, several aspects were kept in consideration. First, 
#  the ball has to have a medium-large gain for the position, as this is what 
#  is telling the ball to move toward center if it's on the edge but with little
#  velocity or tilt. However, with too significant of a position gain, the platform
#  exhibits a large amount of overshoot. To combat this, the velocity gain was 
#  increased. The reasoning behind this is you want the platform to respond to 
#  the increasing velocity in order to slow the ball. This essentially increases
#  the daming in the system and aims to prevent as much overshoot.\n\n
#  
#  While increasing velocity helps prevent overshoot, the next issue is that the
#  platform will be tilted too much, so the ball will be quickly changing between
#  large positive/negative velocities. Thus, the platform tilt gain must be large
#  in order to try and keep the platform level. This helps reduce the possibility
#  of a very large velocity, too.\n\n
#  
#  @section timingTuning Adjusting Timing
#  As expected, having a faster controller helps improve the reaction time
#  of the system, however, too fast of a controller does the opposite. If tasks
#  are being run faster than they can be completed, or, if they're run before 
#  the necessary data is recorded, the system slows, and performs worse. Tuning
#  was required in order to find the faster frequencies while not running tasks
#  late.\n\n
#  
#  @section modifyingBoard Improving System hardware
#  Another portion of testing required modifying the platform setup in order to
#  create the best performing system. First, the motor locations and push-rod 
#  lengths were adjusted so that the pushrods were vertical while the motor
#  arms were horizontal. This was a simplifying assumption made within the 
#  calculations of HW0x02, so it felt fitting to do the same on the board.\n\n
#  
#  A belt tensioner was not provided in the lab kit, however, it would have been
#  quite useful. When testing, it could be seen and heard that the belt was 
#  slipping if there was too sudden of a movement, so the team when to the
#  hardware store and created their own, as seen in Figure 15. This fixed the 
#  issue with the belt slipping, however, it added too much stiction to the motors,
#  which reduced their ability for finer movements. Unfortunately, the tensioner
#  was removed, but could be reintegrated if the belt had one more tooth. \n\n
#  
#  @image      html BeltTensioner.png " " width = 22%
#  @details    \n <CENTER> **Figure 15.** Close-up of the belt tensioner. This 
#  used a small ball bearing and a few spacers on a shaft to add tension to 
#  the belt. </CENTER> \n 
#  
#  Another modification was the ball. Provided was a rubber-coated steel ball,
#  however, the rubber seam created a non-spherical surface. Through testing,
#  it was found that the ball would get stuck on the platform simply because
#  it was not round. It also wasquite light, so it would hop off of the platform
#  easier. For these reasons, a steel ball bearing was used, which was much
#  heavier, but spherical. This had much better results, but required new 
#  tuning values.\n\n
#  
#  The last modification included attaching the motor arms to the looser side of 
#  the eccentric pulley. This was done so that less stiction was in the system.\n\n
#  
#  
# Continue to the next section here: \ref finalResults405
#  
##############################################################################  
## @page finalResults405 Final Results: Evaluation of System Performance
#  After thouroughly tuning and calibrating the system, the final result
#  was tested and filmed in order to show the progress of the project. While
#  the ball doesn't return back to its original starting location, it can stay on the board
#  for up to about 2 minutes. This was deemed acceptable, as in order to improve
#  the performance, reduction in the motor stiction and pivot joints would be
#  necesary. Additionally, a belt tensioner would help. While these would be
#  intruiging to investigate, it is outside the scope of the project. The other
#  main change that could improve the system response would have been use of an
#  IMU to more accurately level out the board before attempting to balance the ball.\n\n
#  
#  @section demoVideo Demonstration Video
#  Below is a YouTube video that describes the term project, shows the balancing
#  results, and explains the data collection. Please view the video below:\n\n
#  
# \htmlonly <div style="text-align:center;">
# <iframe width="560" height="315" src="https://www.youtube.com/embed/2ZHXuGXmlrY" 
# title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; 
# clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
# </div>\endhtmlonly
#  
#  
#  @section finalPlots Example Plots Generated by Program
#  As can be seen in the video above, plots were generated in the PC front end
#  to analyze the data. Figures 16 and 17 below show an example of the data
#  collected from the platform and automatically plotted.\n\n
#  
#  @image      html XFigure.png " " width = 45%
#  @details    \n <CENTER> **Figure 16.** Subplots of the X position/velocity and angle/angular velocity in the Y direction. </CENTER> \n 
#  
#  Ideally, these plots would show the ball position and velocity approaching zero,
#  however we were not able to achieve such a controlled response. Instead, these plots
#  suggest evidence that the controller was working properly to take in feedback and
#  change the location of the ball, but could not act to get the ball to settle in a small
#  region on the board as desired.
#
#  @image      html YFigure.png " " width = 45%
#  @details    \n<CENTER> **Figure 17.** Subplots of the Y position/velocity and angle/angular velocity in the X direction. </CENTER> \n 
#  
#  @section conclusions Final Thoughts
#  This project, overall, was a great way to interface with a variety of common hardware
#  and utilize techniques learned in lecture. The culmination of the term project
#  highlighted many new topics from class while trying to balance the ball on 
#  a 2 DOF platform. \n\n
#  
#  In summary of our design, while the ball doesn't return back to 
#  the origin, the platform does a good job at keeping the ball on top of the
#  resistive touch pad for up to several minutes. To improve performance, 
#  hardware would need improvement in an attempt to remove stiction from the 
#  belt system or the pivot joints. While this is possible, it would take some
#  redesigning of the components, which is out of the scope for the students.
#  Additionally, a modified controller design that uses integral control in
#  addition to full state feedback could potentially help decrease the 
#  aggresiveness of the system response. Thank you to Charlie Refvem for 
#  such an educationally interesting and beneficial project, as well as a
#  great two quarters in ME 305 & 405!
#  
#
# Return to the main page for this project: \ref 405_lab0xFF \n
# Return to the site main page:             \ref index
#  