'''
@file       touchPanelTask.py
@brief      Interface with the touch panel driver to track the ball.
@details    This task will call on the touch panel driver in order to find the
            ball's position and velocity. This will be filtered by using an 
            alpha-beta filter, which uses an estimate of the position and a 
            reading of the error to find the velocity and position of the ball.
            \n *See source code here:*  \n
            <a class="custom" href="https://bitbucket.org/ryanmclaug/matt_ryan_shared/src/master/405_Lab0xFF/Driver%20Classes/touchPanelTask.py" 
            target="_blank" rel="noopener noreferrer">Touch Panel Task Source Code</a>
@author     Matthew Frost
@author     Ryan McLaughlin
@date       Originally created on 05/20/21 \n Last modified on 06/03/21
'''

from touchPanelDriver import touchPanelDriver
from utime import ticks_us, ticks_diff
from pyb import Pin
from micropython import const
import micropython
import shares
from math import fabs

class touchPanelTask:
    ''' 
     @brief    Position/velocity finder for the ball.
     @details  This class will allow for the position and velocity of the ball
               to be calculated using the resisive touch pad.
    '''
    ## @brief       The initial state to set up the task.
    S0_INIT       = const(0)  
    ## @brief       Main state to read from the panel
    S1_READ_PANEL = const(1)

    def __init__(self, debugFlag = True):
        ''' 
        @brief    Create a touch panel task.
        @details  This task will be called from the priority scheduler to read 
                  from the resistive touch panel (through touchPanelDriver.py)
                  in order to find the position and velocity of the ball. This
                  data will be shared with shares.py.
        @param    debugFlag Enables or prevents certain print statements from
                  appearing.
        '''
        ## @brief         Create an instance of the touch panel driver.
        #  @details       This object needs the pin values for the xm, xp, ym, and 
        #                 yp ports on the resistive touch panel. These must be 
        #                 passed in as such `Pin.cpu.##`. Also, the length of the
        #                 board, width of the board, and the coordinates of the 
        #                 center location must be passed in.
        self.touchPanel   = touchPanelDriver(debugFlag = True, xm = Pin.cpu.A1, xp = Pin.cpu.A7, 
                          ym = Pin.cpu.A0, yp = Pin.cpu.A6, boardLength = 176,
                          boardWidth = 107, centerXPos = 88, centerYPos = 53)
        ## @brief         Debugging flag for detailed analysis while running.
        self.debugFlag    = debugFlag # used to view debugging print statements
        ## @brief         The last position of the ball (X, Y, Z coordinates).
        self.lastPosition = (0, 0, True)
        
    @micropython.native    
    def tchPanelFcn(self):
        '''
        @brief      State machine for running the touch panel driver.
        @details    This generator is used to run the touch panel task. The 
                    main states are initializing the touch panel (including
                    determining whether or not the ball is on the platform), and
                    running the drivers to filter the position and speed.
        '''
        ## @brief      Contains the information for which state its in.   
        state          = 0 # start in the sample data state
        ## @brief      Counter for determining if the ball is on the platform.
        startUpCounter = 0 
        
        ## @brief  Initial x-position for alpha-beta filter
        x0 = 0
        ## @brief  Initial y-position for alpha-beta filter
        y0 = 0
        ## @brief  Initial z-position
        z0 = False

        
        while True:
            
            if state == self.S0_INIT:
                
                # Check to see if the ball is on the platform
                if self.touchPanel.getZPos():
                    ## @brief   The number of times that the board has been on
                    startUpCounter += 1
                    
                # if startUpCounter is 10 or greater, start the touch panel readings
                if startUpCounter > 10:
                    if self.debugFlag:
                        print('tTask: Ball is on platform')
                    shares.ballOn.put(1)
                    x0, y0, z0 = self.touchPanel.allPositions()
                    ## @brief  Initial x-velocity for alpha-beta filter
                    vx0 = 0
                    ## @brief  Initial y-velocity for alpha-beta filter
                    vy0 = 0
                    ## @brief    The time of the last iteration in microseconds.
                    lastTime     = ticks_us()
                    state = self.transitionTo(state, self.S1_READ_PANEL)
               
            elif state == self.S1_READ_PANEL:
                
                if shares.ballOn.get() == 0:
                    if self.debugFlag:
                        print('tTask: reset touch task')
                    startUpCounter = 0 
                    state = self.transitionTo(state, self.S0_INIT)
                
                ## @brief   The current time in microseconds.
                currentTime = ticks_us()
                
                ## @brief      The current X,Y,Z from the panel
                currentPosition = self.touchPanel.allPositions()
                
                if self.debugFlag:
                    print('tTask: cur pos = {:}'.format(currentPosition))
                
                # if the positions are valid, start to filter
                if fabs(currentPosition[0]) < 85 and fabs(currentPosition[1]) < 50 and currentPosition[2] == True:     # position reading is valid
                    
                    # Find delta time since last computatation
                    ## @brief  Time between computations of alpha-beta filter values
                    dt = ticks_diff(currentTime, lastTime)/1000000 # microseconds
                    
                    # get X Position and Velocity
                    ## @brief  Next, estimated x-position from alpha-beta filter
                    x1 = x0 + 0.85*(currentPosition[0] - x0) + (vx0*dt)
                    ## @brief  Next, estimated x-velocity from alpha-beta filter
                    vx1 = vx0 + (0.005/dt)*(currentPosition[0] - x0)
                    x0 = x1
                    vx0 = vx1
                    
                    # get Y Position and Velocity
                    ## @brief  Next, estimated y-position from alpha-beta filter
                    y1 = y0 + 0.85*(currentPosition[1] - y0) + (vy0*dt)
                    ## @brief  Next, estimated y-velocity from alpha-beta filter
                    vy1 = vy0 + (0.005/dt)*(currentPosition[1] - y0)
                    y0 = y1
                    vy0 = vy1

                    # reset lastTime
                    lastTime = currentTime
                    
                    # send to shares.py
                    shares.x.put(x1)    
                    shares.y.put(y1) 
                    shares.xDot.put(vx1)  
                    shares.yDot.put(vy1)

            yield(state)
    
    @micropython.native
    def transitionTo(self, state, newState):      # state transition method
        '''
        @brief      Method for transitioning states.
        @details    The method will reassign the `state` variable when
                    directed to transition.
        @param      state The current state of the system.
        @param      newState The new desired state for the system.
        @return     The new state of the finite state machine.
        '''
        if self.debugFlag:
            print('tTask: S' + str(state) + '->' + 'S' + str(newState) )
        return newState               # now that transition has been declared, update state
    