'''
@file       encoderDriver.py
@brief      In charge of keeping track of the position and speed of encoder.
@details    This script implements an algorithm that accounts for overflow and
            underflow in the encoder tick values. From this, the position, in 
            degrees, and speed, in rpm, can be computed and sent back to the
            controller task. Other features are instantaneously getting the 
            position, speed, and delta, as well as zeroing the encoder.
            *See source code here:* 
            <a class="custom" href="https://bitbucket.org/ryanmclaug/matt_ryan_shared/src/master/405_Lab0xFF/Driver%20Classes/encoderDriver.py" 
            target="_blank" rel="noopener noreferrer">Encoder Driver Source Code</a> 
@author     Matthew Frost
@author     Ryan McLaughlin
@date       Originally created on 05/19/21 \n Last modified on 06/03/21
'''
from pyb import Timer, Pin
from micropython import const
import micropython

class encoderDriver:
    ''' 
     @brief    Encoder class to allow for several encoders to be used.
     @details  This class will allow for the position, delta, and speed of
               the encoders to be tracked.
    '''
    def __init__(self, debugFlag = True, pin1 = Pin.cpu.B6, pin2 = Pin.cpu.B7, encTimer = 4):
        ''' 
        @brief    Create an encoder object to find position and speeds.
        @details  This object will be called in order to find the
                  current positions, deltas, speeds, or to reset the
                  encoder back to zero.
        @param    pin1 Input pin to use for the first channel. User must input
                  the exact pin name, for example, Pin.cpu.B6 for encoder 1 or
                  Pin.cpu.C6 for encoder 2.
        @param    pin2 Input pin to use for the second channel. User must input
                  the exact pin name, for example, Pin.cpu.B7 for encoder 1 or
                  Pin.cpu.C7 for encoder 2.
        @param    encTimer Timer number that will be used for the encoder. 
                  Possible options include timer 4 and 8.
        @param    debugFlag Enables or prevents certain print statements from
                  appearing.
       '''
        ## @brief           Debugging flag for detailed analysis while running
        self.debugFlag      = debugFlag 
        ## @brief           The period of the counter (maximum, 0xFFFF)
        self.period         = const(65535)
        ## @brief           Create a timer object
        self.TIM4           = Timer(encTimer, prescaler = 0, period = self.period)
        ## @brief           The first channel for the timer
        self.tim4ch1        = self.TIM4.channel(1, mode = Timer.ENC_AB, pin = pin1)
        ## @brief           The second channel for the timer
        self.tim4ch2        = self.TIM4.channel(2, mode = Timer.ENC_AB, pin = pin2)
        ## @brief           Use the timer to get the current count for the tick
        self.currentTick    = self.TIM4.counter()
        ## @brief           Create a variable for the previous tick
        self.previousTick   = self.currentTick
        ## @brief           Converts ticks into radians (PPR = 4000)
        self.theta          = self.currentTick*.0015708



        
    @micropython.native
    def update(self): 
        '''
        @brief      Update the position and velocity of the encoders.
        @details    This method updates the position (in ticks and radians) 
                    as well as the speed of the motor whenever called. It will
                    return the angle in radians and speed in rad/s.
        '''
        currentTick = self.TIM4.counter() # find the current tick of the encoder
        deltaTick = currentTick - self.previousTick # find delta
        
        # algorithm to account for overflow or underflow
        if deltaTick > 0.5*self.period:
            deltaTick -= self.period
        elif deltaTick < -0.5*self.period:
            deltaTick += self.period
        
        # Compute position in rad
        self.theta += deltaTick*.0015708 # rad
        
        # Record the current tick for the next iteration as previousTick
        self.previousTick = currentTick
        
        return self.theta
        
    
    @micropython.native    
    def getPosition(self):
        '''
        @brief      Get the position of the encoder.
        @details    When called, this function will output the current angle
                    of the encoder in radians.
        @return    The current position of the encoder.
        '''
        if self.debugFlag: 
            print('ENC: sending CtrlTask the current position of: ' + str(self.theta))
        return self.theta 
        
    @micropython.native
    def setPosition(self, newPosition):
        '''
        @brief      Zero the encoder.
        @details    When called, this will set theta to 0, thus resetting the
                    encoder.
        @param      newPosition The specified position to zero the encoder to.
        @return     The new angle for the encoder.
        '''
        self.theta = newPosition
        if self.debugFlag: 
            print('ENC: Position has been set to: ' + str(self.theta))
        return self.theta
    
    @micropython.native
    def getSpeed(self):
        '''
        @brief      Get the speed of the encoder.
        @details    When called, this function will output the current speed
                    of the encoder in rpm.
        @return     The current speed of the encoder.
        '''
        if self.debugFlag: 
            print('ENC: sending CtrlTask the current speed of: ' + str(self.speed))
        return self.speed 
        
