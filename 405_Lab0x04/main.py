'''
@file       main.py
@brief      
@details    
            \n *See source code here:*  \n

@author     Ryan McLaughlin
@author     Matt Frost
@date       Originally created on 05/06/21 \n Last modified on 05/06/21
'''


from mcp9808 import mcp9808

if __name__ == "__main__":
    
    sensorTask = mcp9808(debugFlag = True)

    while True: 
        try: 
            sensorTask.run()

            
        except KeyboardInterrupt:
            print('Ctrl-C has been pressed')
            # break on ctrl-C
            break