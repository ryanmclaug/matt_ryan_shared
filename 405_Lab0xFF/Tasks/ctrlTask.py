'''
@file       ctrlTask.py
@brief      This task runs the full-state feedback controller.
@details    This task will read data from shares.py to find the ball's position
            and velocity, as well as the platforms angular displacement and 
            velocity in order to set the motors to a duty cycle. The math to
            support this is included in Jupyter notebooks that solve the 
            control system for the full-state feedback controller. 
            \n *See source code here:*  \n
            <a class="custom" href="https://bitbucket.org/ryanmclaug/matt_ryan_shared/src/master/405_Lab0xFF/Driver%20Classes/ctrlTask.py" 
            target="_blank" rel="noopener noreferrer">Controller Task Source Code</a>
@author     Ryan McLaughlin
@author     Matthew Frost
@date       Originally created on 05/21/21 \n Last modified on 06/05/21
'''
from micropython import const
import shares
import micropython 
from utime import ticks_us

class ctrlTask:
    ''' 
     @brief    Controller for the system.
     @details  This class implements the full-state feedback control algorithm
               to balance the platform with the ball in the center.
    '''
    ## @brief             The initial state to set up the task.
    S0_INIT               = const(0) 
    ## @brief             State to allow the platform to initially be balanced.
    S1_BALANCE_PLATFORM   = const(1)
    ## @brief             Main state to run the controller and balance the ball.
    S2_BALANCE_BALL       = const(2)
    ## @brief             State to turn off motors (as specified or if balanced).
    S3_STOP_MOTORS        = const(3)
    
    def __init__(self, kMatrixX, kMatrixY, debugFlag = True):
        ''' 
        @brief    Create a controller task.
        @details  This task will be called from the priority scheduler to read 
                  from shares.py in order to get the ball's newest position and
                  velocity as well as the platform's angle and angular velocity.
        @param    kMatrixX K matrix for the x motor for the full-state
                  feedback controller. 
        @param    kMatrixY K matrix for the y motor for the full-state
                  feedback controller.           
        @param    debugFlag Enables or prevents certain print statements from
                  appearing.
        '''
        ## @brief         Debugging flag for detailed analysis while running.
        self.debugFlag    = debugFlag
        ## @brief         Conversion factor for going from torque to duty cycle.
        self.kPrimeFactor = const(334)
        ## @brief         K matrix for motor X as derived analytically and tuned.
        self.kMatrixX     = kMatrixX
        ## @brief         K matrix for motor X as derived analytically and tuned.
        self.kMatrixY     = kMatrixY
        ## @brief         Threshold to determine if the ball is balanced in the center (mm)
        self.xThresh      = const(3)
        ## @brief         Threshold to determine if the ball is balanced in the center (mm)
        self.yThresh      = const(3)
        ## @brief         A flag to determine if we're saving x or y data
        self.dataToSave   = shares.dataToSave
    
    @micropython.native    
    def ctrlTaskFcn(self):
        '''
        @brief      State machine for running the controller.
        @details    This generator is used to run the controller. It will read
                    commands from the uiTask as well as send new duty cycles
                    to the motors. 
        '''
        ## @brief     The corrected K value for multiplying against the x position.
        K1X           = self.kPrimeFactor*self.kMatrixX[0]
        ## @brief     The corrected K value for multiplying against the y theta.
        K2X           = self.kPrimeFactor*self.kMatrixX[1]
        ## @brief     The corrected K value for multiplying against the x velocity.
        K3X           = self.kPrimeFactor*self.kMatrixX[2]
        ## @brief     The corrected K value for multiplying against the y angular velocity.
        K4X           = self.kPrimeFactor*self.kMatrixX[3]
        
        ## @brief     The corrected K value for multiplying against the y position.
        K1Y           = self.kPrimeFactor*self.kMatrixY[0]
        ## @brief     The corrected K value for multiplying against the x theta.
        K2Y           = self.kPrimeFactor*self.kMatrixY[1]*-1
        ## @brief     The corrected K value for multiplying against the y velocity.
        K3Y           = self.kPrimeFactor*self.kMatrixY[2]
        ## @brief     The corrected K value for multiplying against the x angular velocity.
        K4Y           = self.kPrimeFactor*self.kMatrixY[3]*-1

        ## @brief     Contains the information for which state its in.   
        state         = self.S0_INIT
        ## @brief     Flag to determine if x or y data will be stored
        saveDataFlag  = True
        
        while True: 
            
            if state == self.S0_INIT:
                
                if shares.cmd.any():
                    ## @brief  The current command as sent from the uiTask
                    currentCmd = shares.cmd.get()
                    
                    if currentCmd == 98:
                        print('"b" has been pressed, ready to balance the ball\n')
                        state = self.transitionTo(state, self.S1_BALANCE_PLATFORM)
                    elif currentCmd == 115:
                        print('"s" has been pressed, stopping both motors\n')
                        state = self.transitionTo(state, self.S3_STOP_MOTORS)
            
            elif state == self.S1_BALANCE_PLATFORM:
                
                # If a fault occurs, transition to stop the motors.
                if shares.motFault.get():
                    state = self.transitionTo(state, self.S3_STOP_MOTORS)
                
                # If the user presses an `s`, transition to stop motors.
                if shares.cmd.any():
                    currentCmd = shares.cmd.get()
                    if currentCmd == 115:
                        print('"s" has been pressed, stopping both motors\n')
                        state = self.transitionTo(state, self.S3_STOP_MOTORS)
                    
                # Once the ball has been detected, transition to balancing state    
                if shares.ballOn.get() & 1:   
                    if self.debugFlag:
                        print('cTask: ball on')
                    shares.zeroEnc.put(1) # Send command to zero the encoders
                    state = self.transitionTo(state, self.S2_BALANCE_BALL)

            elif state == self.S2_BALANCE_BALL:
                
                # If a fault occurs, transition to stop the motors.
                if shares.motFault.get():
                    state = self.transitionTo(state, self.S3_STOP_MOTORS)

                # If the user presses an `s`, transition to stop motors.
                if shares.cmd.any():
                    currentCmd = shares.cmd.get()
                    if currentCmd == 115:
                        print('"s" has been pressed, stopping both motors\n')
                        state = self.transitionTo(state, self.S3_STOP_MOTORS)

                # motor X, related to x and theta_y
                ## @brief The current x position of the ball        
                cx        = shares.x.get()
                ## @brief The current y theta of the platform (as read by encoders)
                cthY     = shares.thYE.get()  
                ## @brief The current x velocity of the ball
                cxDot     = shares.xDot.get()  
                ## @brief The current y angular velocity of the platform (as read by encoders)
                cthDotY  = shares.thDotYE.get()    
                
                # motor Y, related to y and theta_x
                ## @brief The current y position of the ball
                cy        = shares.y.get()
                ## @brief The current x theta of the platform (as read by encoders)
                cthX     = shares.thXE.get()
                ## @brief The current y velocity of the ball
                cyDot     = shares.yDot.get()
                ## @brief The current x angular velocity of the platform (as read by encoders)
                cthDotX  = shares.thDotXE.get()
                
                # Save either x or y data to shares.py
                if saveDataFlag:
                    shares.time.put(ticks_us())
                    if self.dataToSave == 'x':
                        shares.xData.put(cx)
                        shares.thYData.put(cthY)
                        shares.xDotData.put(cxDot)
                        shares.thDotYData.put(cthDotY)
                    else:
                        shares.yData.put(cy)
                        shares.thXData.put(cthX)
                        shares.yDotData.put(cyDot)
                        shares.thDotXData.put(cthDotX)
                
                # Toggle the saveDataFlag so that every-other data point is stored
                saveDataFlag = not saveDataFlag
                
                # determine new duty cycle values
                ## @brief The new duty cycle to be applied to motor X
                newDutyX = (K1X*cx + K3X*cxDot)/1000  + K2X*cthY + K4X*cthDotY
                ## @brief The new duty cycle to be applied to motor Y
                newDutyY = (K1Y*cy + K3Y*cyDot)/1000  + K2Y*cthX + K4Y*cthDotX
                
                # accomodate due to the stiction
                if cx > 0:
                    newDutyX += 6
                else:
                    newDutyX -= 6 # a bit better with 6 than 10
                    
                if cy > 0:
                    newDutyY += 20
                else:
                    newDutyY -= 0
                    
                # if self.debugFlag:
                    # print('cTask: dX = {:.1f}, dY = {:.1f}'.format(newDutyX,newDutyY))
                    
                # Send new duty cycle to motors
                shares.dutyX.put(newDutyX)
                shares.dutyY.put(newDutyY)

            elif state == self.S3_STOP_MOTORS:
                
                # if user wants to stop the platform, set the motors to 0 forever
                shares.dutyX.put(0)
                shares.dutyY.put(0)
                
                if shares.cmd.any():
                    currentCmd = shares.cmd.get()
                    if currentCmd == 114:
                        print('"r" has been pressed, restarting the balancing process.\n'
                              'When the board has been properly reset, press "b" and place the ball back on the platform')
                            
                        # resetting stuff
                        shares.ballOn.put(0)
                        
                        state = self.transitionTo(state, self.S0_INIT)
            
            yield(state)
            
    @micropython.native        
    def transitionTo(self, state, newState):      # state transition method
        '''
        @brief      Method for transitioning states
        @details    The method will reassign the `state` variable when
                    directed to transition.
        @param      state The current state of the system.
        @param      newState The new desired state for the system.            
        @return     The new state of the finite state machine.            
        '''
        if self.debugFlag:
            print('cTask: S' + str(state) + '->' + 'S' + str(newState))   
        return newState         # now that transition has been declared, update state