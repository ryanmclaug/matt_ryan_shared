'''
@file       uiTask.py
@brief      Class to run state machine on Nucleo and communicate with Nucleo.
@details    Interface with the Spyder Console window through the serial 
            communication. If a keyboard key has been pressed, it will be sent
            through serial to this script, where it will call for an action, 
            such as starting data collection via the ADC. Data will be sent back 
            through serial to output into the Spyder Console or to plot.
            *See source code here:* 

@author     Matthew Frost
@author     Ryan McLaughlin
@date       Originally created on 05/23/21 \n Last modified on 06/05/21
'''
import pyb
from pyb import UART, USB_VCP
from micropython import const
import micropython
import shares

# turn off REPL for the UART
pyb.repl_uart(None) # disables repl on st-link usb port
## @brief Assign variable to UART port 2
myuart = UART(2)

class uiTask:
    ''' 
     @brief    UI Task interfaces between front end and the serial port.
     @details  This class will take data and report it through serial to the 
               front end. It also receives characters/commands from the front 
               end.
    '''
    ## @brief           Basic init state
    S0_INIT             = const(0)   
    ## @brief           Wait for user to press a letter and write to task_share
    S1_WRITE_CMDS       = const(1)   

    def __init__(self, debugFlag = True):
        ''' 
        @brief    Initialize the UI task.
        @details  This task will collect and send data to the front end for 
                  the voltage reading from the button circuitry.
        @param    debugFlag Enables or prevents certain print statements from
                  appearing.
       '''
        ## @brief            Contains the information for which state its in   
        self.state           = 0 # start in the init state
        ## @brief            Debugging flag for detailed analysis while running
        self.debugFlag       = debugFlag # used to view debugging print statements
        
    @micropython.native    
    def uiTaskFcn(self): 
        '''
        @brief      Report necessary measurements the front end.
        @details    This method will wait for a 'g' to be pressed, and then 
                    subsequently take data until a button press has been 
                    identified. It will then send the data through serial to 
                    the front end for further data processing.
        '''
        myVCP = USB_VCP()
        VCPcmd = 0
        
        while True:
                
            if myVCP.any():
                
                VCPcmd = int(myVCP.read(1)[0])
                if self.debugFlag:
                    print('ui: vcp={:}'.format(VCPcmd))
                
                if VCPcmd in [98, 114, 115]:       
                    shares.cmd.put(VCPcmd)
                else:
                    if self.debugFlag:
                        print('uiTask: press valid key')

            yield(0)    
    
    @micropython.native
    def transitionTo(self, state, newState):      # state transition method
        '''
        @brief      Method for transitioning states
        @details    The method will reassign the `state` variable when
                    directed to transition.
        @param      state The current state of the system.
        @param      newState The new desired state for the system.
        @return     The new state of the finite state machine.             
        '''
        if self.debugFlag:
            print('uiTask: S' + str(state) + '->' + 'S' + str(newState) )
        return newState               # now that transition has been declared, update state