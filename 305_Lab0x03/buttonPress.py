'''
@file       buttonPress.py
'''

import pyb

def onButtonPressFCN(IRQ_src): 
    
    print('entering button function')
    global buttonPress

    
    buttonPress = not buttonPress
    print(buttonPress)
    
    
    
if __name__ == "__main__":
    
    print('starting')
    
    buttonPress = False        # initially button is not pressed
           
    pinC13 = pyb.Pin (pyb.Pin.cpu.C13)                      # create pin object for PC13, blue button    
    pinA5 = pyb.Pin (pyb.Pin.cpu.A5)                        # create pin object for PA5, LED
    tim2 = pyb.Timer(2, freq = 20000)                       # define pin timer
    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)       # create timer channel

    # "mode = pyb.ExtInt.IRQ_FALLING|pyb.ExtInt.IRQ_RISING" is the line that changed from last lab
    ButtonInt = pyb.ExtInt(pinC13, mode = pyb.ExtInt.IRQ_FALLING|pyb.ExtInt.IRQ_RISING, pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)

    while True:
        try:

           
           pass 


        except KeyboardInterrupt:       # press Ctrl-C to interupt 
            print('Ctrl-C has been pressed, exiting program')
            t2ch1.pulse_width_percent(0)          
            break