'''
@file       touchPanelCalibration_main.py
@brief      Calibration program for the touch panel driver
@details    This main program utilizes the calibration method of the touchPanelDriver.py
            class file. Since calibration does not need to be completed every time
            the controller is tested, it made sense to create a seperate file for this
            process. The goal of this program is to have a robust method for determining 
            a best fit line for converting ADC readings to actual position (in mm) on the board.
            
            *See source code here:* 
            <a class="custom" href="https://bitbucket.org/ryanmclaug/matt_ryan_shared/src/master/405_Lab0xFF/Driver%20Classes/touchPanelCalibration_main.py" 
            target="_blank" rel="noopener noreferrer">Touch Panel Calibration Source Code</a> 
@author     Matthew Frost
@author     Ryan McLaughlin
@date       Originally created on 05/29/21 \n Last modified on 05/30/21
'''

from pyb import Pin, USB_VCP
import micropython
from touchPanelDriver import touchPanelDriver

micropython.alloc_emergency_exception_buf(100)


if __name__ == "__main__":
    
    ## @brief         Create an instance of the touch panel driver.
    #  @details       This object needs the pin values for the xm, xp, ym, and 
    #                 yp ports on the resistive touch panel. These must be 
    #                 passed in as such `Pin.cpu.##`. Also, the length of the
    #                 board, width of the board, and the coordinates of the 
    #                 center location must be passed in. For the purposes of calibration,
    #                 these values will not be used, but are neccesary to the 
    #                 definition of an object of this class
    tchPanelDriver = touchPanelDriver(debugFlag = True, xm = Pin.cpu.A1, xp = Pin.cpu.A7, 
                          ym = Pin.cpu.A0, yp = Pin.cpu.A6, boardLength = 176,
                          boardWidth = 107, centerXPos = 88, centerYPos = 53)
    
    ## @brief    Virtual Comms Port object
    #  @details  A VCP object is used to allow for user input during program operation.
    #            In most cases, non blocking code is neccesary, however for calibration
    #            timing is not essential, but rather it is desired to stop program operation
    #            until the user accepts a calibration run as accurate or an outlier.
    #            by using `while not myVCP.any():`, the program stops until `myVCP.any()`
    #            returns True (the user presses a keyboard key).
    myVCP = USB_VCP() 
    
    
    ## @brief    Initialize a list for final results
    #  @details  The final results include the following values: `[ mx, xc, my, yc ]`
    #            where mx and my represent slopes of best fit lines, and xc, xy represent
    #            intercepts. These can be directly used by the touch panel driver class.
    results = [0]*4
    
    while True: 
        try: 
            
            ## @brief     Number of calibration trials
            #  @details   The user can change this value to run more or less calibration
            #             trials to average from
            numTests = 5
            ## @brief     Counter for how many trials have been accepted by the user
            success = 0
            ## @brief     List of lists containing calibration data
            #  @details   After each calibration run, the user is instructed to accept
            #             or decline to add the run to the collection of data to be averaged
            #             for final calibration data. Each list has four elements, and the 
            #             lists are contained in a parent list for looping through later on
            calValues = [[0]*4 for i in range(numTests)]
            
            for i in range(numTests):
                _calValuesI = tchPanelDriver.calibration()
                print('Run {:}: mx = {:}, xc = {:}, my = {:}, yc = {:}'.format((i+1),*_calValuesI))
                # pause to wait for user input from keyboard
                print('Press "y" to accept this run, or any other key to decline')
                while not myVCP.any():
                    pass
                if int(myVCP.read(1)[0]) == 121:   # y for yes
                    calValues[i][0:3] = _calValuesI  # add data to list
                    success += 1
                    print('Keeping this run')
                 
                
            calValues.remove([0]*4)
            print(calValues)
            
            
            results = [sum(x) / len(x) for x in zip(*calValues)]
            print('Averages from {:}/{:} succesful runs: mx = {:}, xc = {:}, my = {:}, yc = {:}\n'.format(success,numTests,*results))
            
            # determine whether to continue calibration or exit
            print('Press "r" to restart calibration, and any other key to exit.')
            while not myVCP.any():
                pass
            
            if int(myVCP.read(1)[0]) == 114:   # r for restart
                pass
                print('restarting the calibration process')
            else:
                break

        except KeyboardInterrupt:
            print('Ctrl-C has been pressed, exiting program.')
            break    