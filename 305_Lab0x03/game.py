'''
@file       game.py
@brief      Class file for Nucleo says game
@details    In desinging a Simon Says type game, we decided to implement a single class to
            contain all methods for gameplay. This includes methods for generating morse code
            patterns, blinking an LED on the Nucleo Board, keeping score, and a few others related
            to the workflow of code.
            
            \n *See source code here:* https://bitbucket.org/ryanmclaug/matt_ryan_shared/src/master/game.py \n
            \n *See video here:* https://cpslo-my.sharepoint.com/:f:/g/personal/mtfrost_calpoly_edu/EsKFRc7tdtxCiAGKCC9sRbIBcAyKhIyExQYC9cMowkjBDw?e=pPF0xg
@image      html Lab3_State_Transition.jpg " " width = 80%
@details    \n <CENTER> **Figure 3.1.** State Transition Diagram </CENTER> \n
@image      html lab3CommitHistory.jpg " " width = 80%
@details    \n <CENTER> **Figure 3.2.** BitBucket commit history </CENTER> \n
@author     Ryan McLaughlin
@author     Matthew Frost
@date       Originally created on 02/04/21 \n Last modified on 02/15/21
'''

import utime
import random
import pyb
import sys


'''
@brief      Class for Nucleo says game
@details    See file description for details on this class
'''
class game:
    
    ## @brief Morse code dictionary
    CODE = {'A': '._-',       'B': '-_._._.',   'C': '-_._-_.', 
            'D': '-_._.',     'E': '.',        'F': '._._-_.',
            'G': '-_-_.',     'H': '._._._.',   'I': '._.',
            'J': '._-_-_-',   'K': '-_._-',     'L': '._-_._.',
            'M': '-_-',       'N': '-_.',       'O': '-_-_-',
            'P': '._-_-_.',   'Q': '-_-_._-',   'R': '._-_.',
            'S': '._._.',     'T': '-',         'U': '._._-',
            'V': '._._._-',   'W': '._-_-',     'X': '-_._._-',
            'Y': '-_._-_-',   'Z': '-_-_._.'
        }
    
    # Define all the states within the FSM, static variables
    ## @brief basic init state
    S0_INIT                 = 0
    ## @brief     Countdown state before blinking LED
    S1_COUNTDOWN            = 1
    ## @brief     Blink n = 1 to n = i letter
    S2_BLINK_LED            = 2   
    ## @brief     Waiting state after pattern is blinked
    S3_READY_FOR_INPUT      = 3   
    ## @brief     Counting time that button has been pressed
    S4_BUTTON_PRESSED       = 4    
    ## @brief     Post-processing state for button press
    S5_PROC_BUTTON_PRESSED  = 5
    ## @brief     Transition state between button presses
    S6_BUTTON_RELEASED      = 6
    ## @brief     Post process button released time
    S7_PROC_BUTTON_RELEASED = 7
    ## @brief     User inputted incorrectly, show score/ask to play again
    S8_INCORRECT_INPUT      = 8
    ## @brief     End game if user doesn't want to play
    S9_END_GAME             = 9   
    ## @brief     User has won the set, display game information
    S10_USER_WON            = 10
    
    # constructor definition
    
    def __init__(self, unit, tol, setLength = 5, maxTime = 7000000, debugFlag = False):      # setLength is how many sets per round
        '''
        @brief      Constructor for class `game.py`
        @details    Class constructor definition allows us to define an instance of the class
                    from within the main script, and allows for easy adjustment of certain game-defining
                    parameters, such as the number of sets per round, the max time before the game times out,
                    and others outlined below in variable documentation.
        '''
        ## @brief       Debugging flag
        #  @details     When code is in the design and testing phase, we want a simple way of showing
        #               more print statements in the terminal for debugging
        self.debugFlag = debugFlag
        ## @brief       Timewise defintion of one unit
        #  @details     In using morse code standards for LED pattern creation, letters, spaces, and other elements
        #               are all based off the definition of one unit.
        self.unit = unit        # unit definition for morse code
        ## @brief       Tolerance on user input
        #  @details     Since this game is being played by human users, it is expected that some error is inherent in replicating
        #               the LED pattern, therefore a bi-lateral tolerance is defined for user inputs.
        self.tol = tol          
        ## @brief       Number of sets per round      
        self.setLength = setLength          
        
        ## @brief      Pin attribute for blue user button 
        #  @details    The blue button on the board serves as the main user input interface,
        #              and therefore needs a pin assignment.    
        self.pinC13 = pyb.Pin (pyb.Pin.cpu.C13)      # define pin stuff
        ## @brief      Pin attribute for LED 
        #  @details    Similar to `pinC13`, but for the output LED. 
        self.pinA5 = pyb.Pin (pyb.Pin.cpu.A5) 
        ## @brief       Pin attribute for pyb timer     
        #  @details     To use pulse width modulation of the LED, we need a timer.
        self.tim2 = pyb.Timer(2, freq = 20000)
        ## @brief       Channel attribute for pyb timer
        self.t2ch1 = self.tim2.channel(1, pyb.Timer.PWM, pin=self.pinA5)      
        ## @brief       Callback interrupt attribute
        #  @details     When a rising or falling edge is detected by the button, the interupt will be triggered.
        self.ButtonInt = pyb.ExtInt(self.pinC13, mode=pyb.ExtInt.IRQ_FALLING|pyb.ExtInt.IRQ_RISING, 
                                    pull=pyb.Pin.PULL_NONE, callback=self.onButtonPressFCN)
        
        ## @brief       Counter variable for what LED should be doing
        #  @details     `blinkIndex` is used by the `ledBlink` method to determine how much of the morse
        #               code sequence has already been ouputted through the LED
        self.blinkIndex = 0                 # led blinking iteration variable
        ## @brief       Finite state machine needs a state variable to regularly update as state transitions occur.
        self.state = 0                      # set the state to init
        ## @brief       Round counter attribute
        #  @details     Game begins on the first round every time
        self.round = 1                      # start in the first round 
        ## @brief       Keeps track of the current set of the game
        self.currentSet = 1                 # start in the first set
        ## @brief       Counter variable for the current element
        #  @details     Increments through the number of elements within 
        #               the pattern, which would include dots, dashes, and spaces
        #  @details     Game begins on round 1, and every time a win or loss occurs a round has been completed.
        self.round = 1                      # start in the first round 
        ## @brief       Set counter attribute
        #  @details     Similar to the round counter, there are a fixed number of sets to cycle through in each round.
        self.currentSet = 1                 # start in the first set
        ## @brief       Morse code element counter
        #  @details     To compare user input with LED expected sequence, `elementNum` defines which element within the morse
        #               code sequence is currently being inputted.
        self.elementNum = 0                 # starts at the first element of the pattern
        ## @brief       Define the button as False (released)
        self.buttonPress = False            # button initially not pressed
        ## @brief       Attribute to see if button has been released
        #  @details     Will be utilized to see if the button has been released
        #               before being pressed again
        self.haveReleased = False           # flag that determines if user has released the button
        ## @brief       Attribute to see if new round should start
        #  @details     Will be set to True when user has beat Nucleo for an entire round
        self.newRound = True
        
        # timing setup
        ## @brief       Current time through the entire program
        self.currentTime = utime.ticks_us() # define the current time
        ## @brief       Time in microseconds for when the button has been triggered
        self.buttonTime = self.currentTime  # set the button time to currrent time
        ## @brief       Defines the length of the countdown
        #  @details     The countdown will be defined as counting from 3 to 0
        self.countdown = 3                  # define a 3 second countdown
        ## @brief       Timer counter for the countdown
        #  @details     This counter will keep track of the elapsed time in order
        #               determine how long to display a specific values (between 3 and 0)
        self.countdownElapsedTime = self.currentTime # start the countdown timer at current time
        ## @brief       Define the maximum time before program times out
        #  @details     The program will time out if the user doesn't respond 
        #               within the maxTime attribute
        self.maxTime = maxTime              # the maximum amount of time before 
                                            # the program boots you out for 
                                            # taking too long (in seconds)
        
        # scoring set up
        ## @brief       The nucleo's combined score
        self.nucleoScore = 0                # starting score for the nucleo
        ## @brief       The user's combined score
        self.userScore   = 0                # starting score for the user 
        
    # main run method  
    def run(self):
        '''
        @brief      Finite state machine for the Nucleo Says game
        @details    following the flow of the state transition diagram, this 
                    method controls the finite state machine for the Nucleo
                    Says game. For full documentation on the states and their
                    corresponding transition conditions, please refer to the 
                    state transition diagram provided in the project
                    description.
        '''
        # timing setup
        self.currentTime = utime.ticks_us()

        if self.debugFlag == True:
              print('STATE: ' + str(self.state) + ', ROUND: ' + str(self.round) + ', SET: ' + str(self.currentSet) )

        if self.state == self.S0_INIT:
  
            if self.debugFlag == True:
                print('S0, INIT state')
            
            print('Countdown for new LED pattern is starting, direct your attention to the Nucleo board now.')
            ## @brief   Refrence point for LED blinking
            self.zeroTime = self.currentTime
            self.reset(countdownReset = True, haveReleasedReset = True)
            self.transitionTo(self.S1_COUNTDOWN)
            
        elif self.state == self.S1_COUNTDOWN: 
            # start a countdown timer (not using code blocking, just based on time)
            self.countdownElapsedTime = utime.ticks_diff(self.currentTime, self.zeroTime)
            
            if self.countdown >= 1:
                self.countdownDisplay(self.countdownElapsedTime)
            else:
                self.transitionTo(self.S2_BLINK_LED)
            
        elif self.state == self.S2_BLINK_LED:
 
            # blink LED code goes here:
            ## @brief   Morse code sequence for current set
            self.currentMorse = self.morseGenerator(self.currentSet, self.newRound, self.setLength, self.debugFlag)

            ## @brief   Time since LED pattern started
            self.ledElapsedTime = utime.ticks_diff(self.currentTime, self.zeroTime)
            if self.debugFlag == True:
                print('time elapsed is ' + str(self.ledElapsedTime))
                print('the value for blinkIndex is ' + str(self.blinkIndex))
                
            self.ledBlink(self.currentMorse, self.ledElapsedTime, self.blinkIndex)
            if self.debugFlag == True:
                print('the value for blinkIndex is ' + str(self.blinkIndex))
                print('the value for buttonPress is ' + str(self.buttonPress))
            
            # blink index starts at zero (since list elements start at zero)
            # so if blinkIndex is at length of currentMorse pattern, pattern is complete
            if self.blinkIndex == len(self.currentMorse):         
                print('Ready for user input of pattern')
                    
                self.reset(blinkReset = True,ledReset = True)   
                self.transitionTo(self.S3_READY_FOR_INPUT)

        elif self.state == self.S3_READY_FOR_INPUT:  
        # if button is pressed, transition to the user input code (S2)
            if self.buttonPress == True:
                self.buttonTime = self.currentTime
                if self.debugFlag == True:
                    print('Button was pressed at: ' + str(self.buttonTime))
                self.transitionTo(self.S4_BUTTON_PRESSED)

        elif self.state == self.S4_BUTTON_PRESSED:
            # determine the time that the button has been held down for
            ## @brief  	Time that the button has been pressed for.
            self.elapsedDownTime = utime.ticks_diff(self.currentTime, self.buttonTime)
            
            if self.debugFlag == True and self.buttonPress == True:
                print('S2, button is being held down')
                # see amount of time pressed
                print('Current time since being pressed is: ' + str(self.elapsedDownTime)) 
            
            # if button is released, transition to the processing state
            if self.buttonPress == False:
                self.buttonTime = self.currentTime
                if self.debugFlag == True:
                    print('Button was released at: ' + str(self.buttonTime))
                self.transitionTo(self.S5_PROC_BUTTON_PRESSED)
            
            # if the user holds the button down for too long, time the program out
            if self.elapsedDownTime >= self.maxTime:
                print('Program timed out, exiting game')
                self.scoringUpdate(self.S8_INCORRECT_INPUT)
                self.transitionTo(self.S8_INCORRECT_INPUT)

                ## @brief   Timestamp for user incorrect input 
                self.restartTime = self.currentTime            

        elif self.state == self.S5_PROC_BUTTON_PRESSED:
            
            if self.debugFlag == True:
                print('Expected input is ' + str(self.currentMorse[self.elementNum])) 
                
            # Convert time elapsed into a dot or a dash representation
            if self.unit - self.tol <= self.elapsedDownTime <= self.unit + self.tol:
                ## @brief  Morse code element equivalent translated from user input
                self.value = '.'
                if self.debugFlag == True:
                    print('Actual input translated to a DOT')
            elif 3*self.unit - self.tol <= self.elapsedDownTime <= 3*self.unit + self.tol:
                self.value = '-'
                if self.debugFlag == True:
                    print('Actual input translated to a DASH')
            else:
                self.value = ' '
                if self.debugFlag == True:
                    print('Actual input is neither a dot or a dash, incorrect')
            
            # compare the dot/dash representation to the expected value
            if self.value == self.currentMorse[self.elementNum]:                       #
                if self.debugFlag == True:
                    print('Hooray! You matched element # '+ str(self.elementNum) +' correctly!')
                    print('morse code pattern length is ' + str(len(self.currentMorse)))
                
                # case 1, element correct, set incomplete, round incomplete
                if self.elementNum < len(self.currentMorse)-1:                                           
                    self.elementNum += 1                                                                     
                    if self.debugFlag == True:
                         print('Element correct, set incomplete. Now looking for element: '  + str(self.elementNum))
                    self.transitionTo(self.S6_BUTTON_RELEASED)
                    
                # case 2, element correct, set complete, round incomplete
                elif self.elementNum == len(self.currentMorse)-1 and self.currentSet < self.setLength:      
                    self.currentSet += 1
                    self.reset(elementReset = True)
                    print('Set complete, moving on to set ' + str(self.currentSet) + ' of ' + str(self.setLength))
                    self.transitionTo(self.S0_INIT)
                         
                # case 3, element correct, set complete, round complete
                else:
                    ## @brief  Timestamp for user winning a round
                    self.winTime = self.currentTime
                    self.scoringUpdate(self.S10_USER_WON)
                    self.transitionTo(self.S10_USER_WON)
                    
            else:  
               self.restartTime = self.currentTime     
               self.scoringUpdate(self.S8_INCORRECT_INPUT)
               self.transitionTo(self.S8_INCORRECT_INPUT)

        elif self.state == self.S6_BUTTON_RELEASED:
            ## @brief  Time that the button has been released for
            self.elapsedUpTime = utime.ticks_diff(self.currentTime, self.buttonTime)
            
            if self.debugFlag == True:
                print('S4, button is currently released')
                # see amount of time pressed
                print('Current time since being released is: ' + str(self.elapsedUpTime))
            
            if self.buttonPress == True:
                self.buttonTime = self.currentTime
                if self.debugFlag == True:
                    print('Button was pressed at: ' + str(self.buttonTime))
                self.transitionTo(self.S7_PROC_BUTTON_RELEASED)
                
            if self.elapsedUpTime >= self.maxTime:
               print('Program timed out, exiting game')
               self.restartTime = self.currentTime
               self.scoringUpdate(self.S8_INCORRECT_INPUT)
               self.transitionTo(self.S8_INCORRECT_INPUT)

        elif self.state == self.S7_PROC_BUTTON_RELEASED:
            # Convert time elapsed into a dot or a dash representation
            if self.unit - self.tol <= self.elapsedUpTime <= self.unit + self.tol:
                self.value = '_'
                if self.debugFlag == True:
                    print('Translated to an ELEMENT SPACE')
            elif 3*self.unit - self.tol <= self.elapsedUpTime <= 3*self.unit + self.tol:
                self.value = '/'
                if self.debugFlag == True:
                    print('Translated to a LETTER SPACE')
            else:
               self.restartTime = self.currentTime
               self.scoringUpdate(self.S8_INCORRECT_INPUT)
               self.transitionTo(self.S8_INCORRECT_INPUT)
 
            # compare the element/letter space representation to the expected value
            # can never end on a space, so don't need to worry about set or round counts here
            if self.value == self.currentMorse[self.elementNum]: 
               # increment the element counter once correct
               if self.debugFlag == True:
                   print('Hooray! You matched the element correctly!')
               
               self.elementNum += 1
               if self.debugFlag == True:
                   print('Now looking for element: '  + str(self.elementNum))
               self.transitionTo(self.S4_BUTTON_PRESSED)

            else:
                self.restartTime = self.currentTime
                self.scoringUpdate(self.S8_INCORRECT_INPUT)
                self.transitionTo(self.S8_INCORRECT_INPUT)

        elif self.state == self.S8_INCORRECT_INPUT:
            
            if self.debugFlag == True:
                print('S6, user has messed up \n')
                print('New Nucleo score is ' + str(self.nucleoScore) + ' \n')
            
            # start a count to see time since incorrect input
            ## @brief  Elapsed time since incorrect input detected
            self.timeSinceRestart = utime.ticks_diff(self.currentTime, self.restartTime)
            
            if self.buttonPress == False:
                self.haveReleased = True
                if self.timeSinceRestart >= self.maxTime: 
                    self.transitionTo(self.S9_END_GAME)
                
            if self.buttonPress == True and self.haveReleased == True:                  
                print('You have selected to play again \n')
                print('You were on set ' + str(self.currentSet) + ' of round ' + str(self.round))
                
                # reset counter variables
                self.reset(setReset = True, roundReset = True, blinkReset = True, elementReset = True )
                self.round += 1             # new round has technically started
                print('Round has been reset, next round will begin shortly')
                # return to blinking LED state to restart
                self.zeroTime = self.currentTime
                self.transitionTo(self.S0_INIT)
                
            elif self.timeSinceRestart >= self.maxTime: 
                self.transitionTo(self.S9_END_GAME)
            
            else:
                if self.debugFlag == True:
                    print('Remaining time before game times out is ' + str( self.maxTime - self.timeSinceRestart))
 
                
        elif self.state == self.S9_END_GAME:        # exit the game
            sys.exit('Thanks for playing Nucleo Says: The ME 305 Original Game. See you next time!')

        elif self.state == self.S10_USER_WON:
            if self.debugFlag == True:
                print('Old user score was ' + str(self.userScore) + ' \n')

            # start a count to see time since user won
            ## @brief  Elapsed time since user won a round
            self.timeSinceWon = utime.ticks_diff(self.currentTime, self.winTime)
            
            if self.buttonPress == True:
                print('You have selected to play again')
                print('You were on set ' + str(self.currentSet) + ' of round ' + str(self.round) )
                
                # reset counter variables
                self.reset(setReset = True, roundReset = True , blinkReset = True, elementReset = True )
                self.round += 1 # new round has technically started
                print('Round has been reset, next round will begin shortly')
                # return to blinking LED state to restart
                self.zeroTime = self.currentTime
                self.transitionTo(self.S0_INIT)
                
            elif self.timeSinceWon >= self.maxTime: 
                self.transitionTo(self.S9_END_GAME)
            
            else:
                if self.debugFlag == True:
                    print('Remaining time before game times out is ' + str(self.maxTime - self.timeSinceWon))

    def transitionTo(self, newState):      # state transition method
        '''
        @brief      Method for transitioning states
        @details    The method will reassign the `self.state` variable when
                    directed to transition.
        '''
        if self.debugFlag == True:
            print('Transitioning from ' + 'S' + str(self.state) + '->' + 'S' + str(newState) )
            print('State transition occured at t = ' + str(self.currentTime) )
        self.state = newState               # now that transition has been declared, update state

    def scoringUpdate(self, newState):     # scoring method
        '''
        @brief      Method for scorekeeping during gameplay
        @details    When transitioning between certain states, the Nucleo or 
                    user score will be incremented to keep tally of who is winning.
                    This method takes in the state being transitioned to, and interprets
                    what the outcome of the round accordingly was.
        '''
        if newState == self.S8_INCORRECT_INPUT:
            print('Incorrect input, user has lost')
            self.nucleoScore += 1
            
        if newState  == self.S10_USER_WON:
            print('Congratulations! You have beat Nucleo for this round \n')
            self.userScore += 1
        print('Total score between Nucleo and User is: ')
        print('Nucleo: ' + str(self.nucleoScore) + ', User: ' + str(self.userScore) + '\n')
        print('If you would like to play again, press the button within ' + str(self.maxTime/1000000 )+' seconds')
                

    def onButtonPressFCN(self, IRQ_src):      # button interupt method
        '''
        @brief      Callback function for the button press
        @details    When the button is pressed, the `onButtonPressFCN` function
                    will be called. In the constructor, `buttonPress` is set to `False`,
                    such that when it is pressed, it switches to `True`. Likewise, since the
                    button is set to detect both rising and falling edges, the button
                    returns to `False` when released. Based on use of the `not` operator.
        '''
        # button is initially set to false in constructor
        # when buttonPress is false, button is unpressed, so to enter the function the button is being pressed (or vice versa)
        # if button is pressed, switch to true, but if released from True, then go back to false
        self.buttonPress = not self.buttonPress

    def ledBlink(self, currentMorse, ledElapsedTime, blinkIndex):    # LED method
        '''
        @brief      Method to blink the LED according to the morse pattern
        @details    The LED will blink based on time without using blocking
                    code. That is, it will be turned on while under a threshold
                    of either a dot or dash amount of time. When finished 
                    displaying either a dot or a dash, it will find the next 
                    element of the current morse code pattern and display that,
                    incrementing until the pattern has been fully displayed
        '''
        if currentMorse[blinkIndex] == '.':
            if ledElapsedTime <= self.unit:
                self.t2ch1.pulse_width_percent(100)
                if self.debugFlag == True:
                    print('should be blinking a dot')
            else:
                self.blinkIndex += 1
                self.zeroTime = self.currentTime

        elif currentMorse[blinkIndex] == '-':       
            if ledElapsedTime <= 3*self.unit:
                self.t2ch1.pulse_width_percent(100)
                if self.debugFlag == True:
                    print('should be blinking a dash')
            else:
                self.blinkIndex += 1
                self.zeroTime = self.currentTime

        elif currentMorse[blinkIndex] == '_':    
            if ledElapsedTime <= self.unit:
                self.t2ch1.pulse_width_percent(0)
                if self.debugFlag == True:
                    print('should not be blinking, element space')
            else:
                self.blinkIndex += 1
                self.zeroTime = self.currentTime

        elif currentMorse[blinkIndex] == '/':    
            if ledElapsedTime <= 3*self.unit:
                self.t2ch1.pulse_width_percent(0)
                if self.debugFlag == True:
                    print('should not be blinking, letter space')
            else:
                self.blinkIndex += 1
                self.zeroTime = self.currentTime   

    def countdownDisplay(self, countdownElapsedTime):   # countdown method
        '''
        @brief      Method to display a countdown
        @details    Before blinking the new LED pattern, a countdown will 
                    be displayed to prep the user for the incoming signal. 
                    This method will display a number (between 3 and 1) until
                    one second has elapsed, in which it will increment to the 
                    next lower number until it gets to 1, where it will 
                    transition to blinking the LED state. This promotes the use
                    of non-blocking code, while having a similar effect on gameplay
                    as a `time.sleep()` variation.
        '''
        if countdownElapsedTime <= 1000000:
            pass
        else:
            print(str(self.countdown))
            self.countdown += -1
            self.zeroTime = self.currentTime

    def morseGenerator(self, currentSet, newRound, setLength, debugFlag):  # pattern generation method
        '''
        @brief      Method for generating the morse code pattern for the round
        @details    If starting a new round, an entirely new pattern will be 
                    randomly selected from a library of letters and corresponding
                    morse code patterns. As a round progresses, `morseGenerator` will
                    be called again, each time refrencing a larger portion of the pre-defined
                    sequence. Only at the start of a new round will the sequence
                    be refreshed, promoting more efficient code.                    
        '''
        # check if need to generate new pattern, or get next value in existing pattern
        if newRound == True:
            # in the case of a new round, need a new pattern of letters
            n = 1
            ## @brief  Full morse code sequence generated at the beginning of a round
            self.pattern = []
            for n in range(setLength):
                self.pattern.append( random.choice( list(self.CODE.keys())) )
                
                if debugFlag == True:
                    print('letters chosen are ' + str(self.pattern) )
        
        self.newRound = False
        
        # need as many letters as the set number is currently at
        ## @brief  Current morse code sequence, pre-proccessing
        self.thisPattern = self.pattern[0:currentSet]
        
        # take string of letters, turn into morse code
        self.currentMorse = '/'.join(self.CODE.get(i.upper()) for i in self.thisPattern)        
        if self.debugFlag == True:
            print(self.currentMorse)
        return(self.currentMorse)   
    
    def reset(self, stateReset = False, setReset = False, 
                    roundReset = False , blinkReset = False, 
                    elementReset = False ,ledReset = False,
                    countdownReset = False, haveReleasedReset = False):
        '''
        @brief      Method to reset common attributes
        @details    Since certain attributes are commonly reset (such as 
                    counters), it was more efficient to write a method to 
                    reset them more efficiently. Depending on which attributes
                    are named as inputs to the method, those specific ones
                    will be reset to whatever value is intended. This way, we do
                    not need to keep track of which variables reset to 0, 1, etc.
        '''
        if stateReset == True:
            self.state = 0
        if setReset == True:
            self.currentSet = 1 
        if roundReset == True:
            self.newRound = True 
        if blinkReset == True:
            self.blinkIndex = 0
        if elementReset == True:
            self.elementNum = 0
        if ledReset == True:
            self.t2ch1.pulse_width_percent(0)
        if countdownReset == True:
            self.countdown = 3
        if haveReleasedReset == True:
            self.haveReleased = False