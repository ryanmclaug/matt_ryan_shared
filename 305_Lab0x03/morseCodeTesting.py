'''
@file       filename.py
@brief      one line description here
@details    detailed description here
            
            \n *See source code here:* <insert link> \n
@image      html filename.jpg “Caption” width = xx% 
@author     Ryan McLaughlin
@date       Originally created on DD/MM/YY \n Last modified on DD/MM/YY
'''

import utime
from morseCode import morseCode

task = morseCode()

setLength = 5
currentSet = 1
roundNum = 1
newRound = True

while True:
    try:
        
        if currentSet <= setLength:
            print('round number ' + str(roundNum) + ', set number ' + str(currentSet) )    
            pattern = task.morseGenerator( currentSet , newRound , setLength ) 
            
            currentSet += 1
            newRound = False      # no longer a new set
            utime.sleep(1)
        
        else:
            currentSet = 1               # reset
            newRound = True       # reset, is a new set next iteration
            utime.sleep(1)
            roundNum += 1
    
    except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard
            # to end the while(True) loop while desired
            break
        
        

