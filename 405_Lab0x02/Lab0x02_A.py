'''
@file       Lab0x02_A.py
@brief      Use code based timing methods to calculate reaction times
@details    Using the Nucleo L476RG board and code/software based methods, 
            the reaction time of a user pressing the B1 button will be measured
            when the LD2 LED turns on. For Part A of this lab, the LED is turned
            on and off using the `high()` and `low()` methods for a pin. The LED
            will be on for 1 second each time (controlled through a state machine),
            and will turn off for a random time between 2 to 3 seconds. Once the user
            sees the LED flash on, they will press the button, triggering a callback
            and starting the sequence of calculating a reaction time. Although
            this method works, it is not the most effective or accurate. Part B of
            the lab covers a hardware based solution that improves upon Part A.
            \n *See source code here:* https://bitbucket.org/ryanmclaug/matt_ryan_shared/src/master/405_Lab0x02/Lab0x02_A.py \n
            
@image      html Lab0x02A_MainFSM.jpg " " width = 30%
@details    \n <CENTER> **Figure 1.** Overall program FSM. </CENTER> \n
@author     Ryan McLaughlin
@author     Matthew Frost
@date       Originally created on 04/22/21 \n Last modified on 04/27/21
'''

import utime
import random
import pyb


def onButtonPressFCN(IRQ_src):      # button interupt method
        '''
        @brief      Callback function for the button press
        @details    When the button is pressed, the `onButtonPressFCN` function
                    will be called. In the constructor, `buttonPress` is set to `False`,
                    such that when it is pressed, it switches to `True`. Likewise, since the
                    button is set to detect both rising and falling edges, the button
                    returns to `False` when released. Based on use of the `not` operator.
        '''
        global buttonPress
        
        # button is initially set to false in constructor
        # when buttonPress is false, button is unpressed, so to enter the function the button is being pressed (or vice versa)
        # if button is pressed, switch to true, but if released from True, then go back to false
        if pinA5.value() == 1:
            buttonPress = True
    

 
    
if __name__ == "__main__":
    ## @brief      Pin attribute for blue user button 
    #  @details    The blue button on the board serves as the main user input interface,
    #              and therefore needs a pin assignment.   
    pinC13 = pyb.Pin (pyb.Pin.cpu.C13)      # define pin stuff
        
    ## @brief      Pin attribute for LED 
    #  @details    Similar to `pinC13`, but for the output LED. 
    pinA5 = pyb.Pin (pyb.Pin.board.PA5, mode = pyb.Pin.OUT_PP) 
    
    ## @brief       Callback interrupt attribute
    #  @details     When a rising or falling edge is detected by the button, the interupt will be triggered.
    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)
    
    ## @brief  Initial state
    S0_INIT                 = 0   # basic init state
    ## @brief  State for pause between LED blinking
    S1_WAITING              = 1   
    ## @brief  State for LED on-time
    S2_LED_ON               = 2 
    
    ## @brief    Variable to keep track if the button is pressed or not
    buttonPress = False        # initially button is not pressed
    
    ## @brief   time to compare to in calculating elapsed time since a certain action
    zeroTime = utime.ticks_us()
    
    ## @brief   Off time between LED flashes
    ledTime = random.randrange(2000000,3000000)
    
    ## @brief   Time interval for LED to stay on
    ledPeriod = 1000000
    
    ## @brief    State machine state variable for mini FSM
    state = 0
    
    ## @brief    List to hold the reaction times (later will be averaged)
    rxnTimes     = []
    
    ## @brief    Variable to display the average of the reaction times
    avgRxnTime    = 0 # The average starts at 0 since no reactions have been recorded
    

    print('Welcome to the `Think Fast!` game! To play the game, start the '
          'Nucleo. As soon as the LED turns on, press the blue button as '
          'fast as possible. Do this for as long as you would like, and when '
          'finished, press Ctrl-C. This will display your average reaction '
          'time for the run. Hope you enjoy!\n')
    
    while True:
        try:
            ## @brief   current time in microseconds
            currentTime = utime.ticks_us()
            
            ## @brief   elapsed time since zeroTime was last reset (microseconds)
            elapsedTime = utime.ticks_diff(currentTime, zeroTime)
               
            if state == S0_INIT:
                state = S1_WAITING
            
            elif state == S1_WAITING:
                
                # this loop is triggered after 2-3 seconds of wait time passes
                if elapsedTime >= ledTime:
                    pinA5.high()
                    # print('LED on')
                    ledTime = random.randrange(2000000,3000000)
                    # print('new LED time selected is {:} microseconds'.format(ledTime))
                    zeroTime = currentTime
                    ## @brief  Should the program calculate a reaction time?
                    calculate = True
                    state = S2_LED_ON
                
            elif state == S2_LED_ON:
                
                if buttonPress == True and calculate == True:
                    
                    ## @brief  current reaction time (microseconds)
                    thisRxnTime = elapsedTime
                    if thisRxnTime > 0:
                        print('this reaction time was {:} microseconds ({:} seconds)'.format(thisRxnTime, thisRxnTime/1000000))
                        rxnTimes.append(thisRxnTime)
                        avgRxnTime = sum(rxnTimes)/len(rxnTimes)
                        print('current average reaction time is now {:} microseconds ({:} seconds)\n'.format(avgRxnTime,avgRxnTime/1000000))
                    
                    calculate = False
                    
                if elapsedTime >= ledPeriod:
                    # print('LED being turned off after {:} microseconds'.format(elapsedTime))
                    pinA5.low()
                    # print('LED off')
                    zeroTime = currentTime
                    buttonPress = False
                    state = S1_WAITING
                    
        except KeyboardInterrupt:       # press Ctrl-C to interupt 
            print('Ctrl-C has been pressed, exiting program')
            # show stats
            if avgRxnTime == 0:
                print('no reaction time was recorded')
            else:
                print('\n--- average reaction time for this session was {:} microseconds ({:} seconds) ---\n'.format(avgRxnTime,avgRxnTime/1000000))
            pinA5.low()
            break

