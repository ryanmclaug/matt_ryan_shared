'''
@file       PCFrontEnd.py
@brief      Set up UI between Spyder and Nucleo to collect and plot data.
@details    Data recorded by various sensors connected to the Nucleo is collected
            and sent through serial to the front end for plotting and writing to
            a .csv file. 
            *See source code here:* 
@author     Ryan McLaughlin
@author     Matthew Frost
@date       Originally created on 05/23/21 \n Last modified on 06/06/21
'''
import serial
import csv
import os 
from matplotlib import pyplot as plt
from matplotlib import rcParams
import numpy as np

## @brief Set up serial communication through serial command
ser = serial.Serial(port='COM5', baudrate=115200, timeout=1)
ser.flushInput()
    
def writeToCSV(data, filename):
    ''' 
    @brief   Strip and split the data into a csv file from the Nucleo.
    @details After data collection is finished, the data must be stripped
             and split in order to write to a csv file.
    @param   data The data from the serial port that needs to be saved into the CSV.
    @param   filename The specific file to save the data to.                 
    '''
    dataStripped = data.strip()
    dataSplit    = dataStripped.split(', ')
    
    timeD     = float(dataSplit[0])/1000000              # seconds
    xD        = float(dataSplit[1])                      # mm
    xDotD     = float(dataSplit[2])                      # mm/s
    thYED     = float(dataSplit[3])*180/3.1415           # deg
    thDotYED  = float(dataSplit[4])*180/3.1415           # deg/s
               
    with open(filename,"a") as f:
        writer = csv.writer(f,delimiter=",")
        writer.writerow([timeD, xD, xDotD, thYED, thDotYED])       

def clearFiles(fileNames):
    '''
    @brief      Method for deleting files
    @details    Since the csv writer is set to append to a certain file, the file to be written to must be removed
                each time the PC collects new data. If it was desired, this could be replaced by a method to rename
                the next file to write to a new file each time. For this, the `os` module is used.
    @param      fileNames is a list of strings containing the various files written each time the frontEnd is run 
    '''    
    _filesRemoved = 0
    
    for n in range(len(fileNames)):
        
        _fileName = fileNames[n]
        
    # check if file exists 
        if os.path.exists(_fileName):
            os.remove(_fileName)
            _filesRemoved += 1
            
    print('{:} .csv/.png files have been removed'.format(_filesRemoved))

def plotData(csvName, figNameX, figNameY):
    ''' 
    @brief   Plot the position and speed data from the encoders.
    @details The subplot feature within matlibplot will be utilized to visualize
             the position and speed of the motors along with the reference 
             data to see the difference in position and speed at all points in
             time.
    @param   csvName   Specifies the file to grab the data from.
    @param   figNameX  Specifies the name of the x-data figure
    @param   figNameY  Specifies the name of the y-data figure
    '''
    timeD,xD, xDotD,thYED,thDotYED = np.loadtxt(csvName, delimiter=',', usecols=(0,1,2,3,4), unpack=True)
    # font = {'fontname':'Times New Roman'}
    rcParams['font.family'] = 'monospace'
    timeD -= timeD[0]
    
    if dataToSave == 'x':
        fig1,axs1 = plt.subplots(2,2,figsize = (6,6), dpi = 600, constrained_layout=True)
        plt.subplots_adjust(hspace = 0.7,wspace = 0.7)
        
        axs1[0,0].plot(timeD, xD,'k',linewidth = 0.75)
        axs1[0,0].set(title='Ball Displacement',xlabel = r'Time, $t$ [ s ]',ylabel = r'$x(t)$ [ mm ]')
        axs1[0,1].plot(timeD, thYED,'k',linewidth = 0.75)
        axs1[0,1].set(title='Platform Angle',xlabel = r'Time, $t$ [ s ]',ylabel = r'${\theta}_y(t)$ [ deg ]')
        axs1[1,0].plot(timeD, xDotD,'k',linewidth = 0.75)
        axs1[1,0].set(title='Ball Velocity',xlabel = r'Time, $t$ [ s ]',ylabel = r'$\dot{x}(t)$ [ mm/s ]')
        axs1[1,1].plot(timeD, thDotYED,'k',linewidth = 0.75)
        axs1[1,1].set(title='Platform Angular Velocity',xlabel = r'Time, $t$ [ s ]',ylabel = r'$\dot{\theta}_y(t)$ [ deg/s ]')
        plt.show()
    
    else:
        fig2,axs2 = plt.subplots(2,2,figsize = (6,6), dpi = 600, constrained_layout=True)
        plt.subplots_adjust(hspace = 0.7,wspace = 0.7)
        
        axs2[0,0].plot(timeD, xD,'k',linewidth = 0.75)
        axs2[0,0].set(title='Ball Displacement',xlabel = r'Time, $t$ [ s ]',ylabel = r'$y(t)$ [ mm ]')
        axs2[0,1].plot(timeD, thYED,'k',linewidth = 0.75)
        axs2[0,1].set(title='Platform Angle',xlabel = r'Time, $t$ [ s ]',ylabel = r'${\theta}_x(t)$ [ deg ]')
        axs2[1,0].plot(timeD, xDotD,'k',linewidth = 0.75)
        axs2[1,0].set(title='Ball Velocity',xlabel = r'Time, $t$ [ s ]',ylabel = r'$\dot{y}(t)$ [ mm/s ]')
        axs2[1,1].plot(timeD, thDotYED,'k',linewidth = 0.75)
        axs2[1,1].set(title='Platform Angular Velocity',xlabel = r'Time, $t$ [ s ]',ylabel = r'$\dot{\theta}_x(t)$ [ deg/s ]')
        plt.show()


## @brief     File name for initial plot
pngFileNameX  = "Lab0xFF_MotXData.png"
## @brief     File name for initial plot
pngFileNameY  = "Lab0xFF_MotYData.png"
## @brief     File name of the exported data
csvFileName   = "Lab0xFF_allData.csv"

# check to see if the csv file exists, and if it does, delete it so it can be
# written over with new data
clearFiles([pngFileNameX,pngFileNameY,csvFileName])

## @brief        Debugging flag to hide certain print statements
debugFlag        = False

## @brief        The outputted data from the serial port
dataFromDataTask = None

## @brief   String that sets whether x or y data is being collected
dataToSave = input('Please input either an x or a y depending on which data is being plotted: ')

while True:
    try:      
        
        ## @brief  Most recent data from serial communciation between PC and dataTask
        dataFromDataTask = ser.readline().decode('ascii')    # read from uart

        print(dataFromDataTask)
        
        if dataFromDataTask:
            if debugFlag == True:
                print('dataFromDataTask is not empty')
            
            ## @brief Non empty line from Nucleo serial communication
            currentFromNucleo = dataFromDataTask
            
            
            if currentFromNucleo == 'plot\r\n':     
                plotData(csvFileName, pngFileNameX, pngFileNameY)
               
               
            else:   # this case is for writing to csv file
                if debugFlag == True:
                    print('writing ' + currentFromNucleo + ' to csv file')
        
                writeToCSV(currentFromNucleo,csvFileName)



    except KeyboardInterrupt:
        plotData(csvFileName, pngFileNameX, pngFileNameY)
        break
    
# closer serial port
ser.close()