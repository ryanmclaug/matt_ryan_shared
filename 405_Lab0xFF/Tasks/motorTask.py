'''
@file       motorTask.py
@brief      Create a motor task to create motor objects and interface with controller.
@details    This file will create two instances of the motorDriver.py class in
            order to send new duty cycles to them. Those duty cycles will be 
            passed in from the controller from shares.py.
            \n *See source code here:*  \n
            <a class="custom" href="https://bitbucket.org/ryanmclaug/matt_ryan_shared/src/master/405_Lab0xFF/Driver%20Classes/motorTask.py" 
            target="_blank" rel="noopener noreferrer">Motor Task Source Code</a>
@author     Ryan McLaughlin
@author     Matthew Frost
@date       Originally created on 05/21/21 \n Last modified on 06/03/21 
'''

from motorDriver import motorDriver
from pyb import Pin, Timer
from micropython import const
import micropython
import shares

class motorTask:
    ''' 
     @brief    Task to call the motor drivers for setting duty cycles.
     @details  This task will be called from the priority scheduler to set the
               duty cycles. This class also will disable the motors under two 
               conditions: a fault occurs, which will cause an interrupt in the
               motor driver, or if the user presses the `s` key.
    '''
    ## @brief       The initial state to set up the task.
    S0_INIT         = const(0)  
    ## @brief       Main state to read data from the motors.
    S1_MOVE_MOTORS  = const(1)
    ## @brief       When the motor hits a fault, the user can restart the program.
    S2_LATCH        = const(2)
    
    def __init__(self, mTaskFlag = False):
        ''' 
        @brief    Create a motor task.
        @details  This task will be called from the priority scheduler to set the
                  duty cycles. This class also will disable the motors under two 
                  conditions: a fault occurs, which will cause an interrupt in the
                  motor driver, or if the user presses the `s` key.
        @param    mTaskFlag Enables or prevents certain print statements from
                  appearing.
        '''
        ## @brief           Debugging flag for detailed analysis while running
        self.mTaskFlag      = mTaskFlag
        ## @brief           Create an instance of the motor driver.
        #  @details         The motor driver contains the functionality for
        #                   both motors. Specific motors will be called using
        #                   the .channel method within the motor driver.
        self.motorDriverObj = motorDriver(Pin.cpu.A15 , Pin.cpu.B2, mDriverFlag = False)
        ## @brief           Create a channel for motor X.
        #  @details         Using the channel method within the motor driver,
        #                   the X motor will be created using channels 3 and 4
        #                   to allow for the X motor to have its own duty cycles.
        self.motX           = self.motorDriverObj.channel(Pin.cpu.B0, Pin.cpu.B1, 
                                                          Timer(3, freq=20000), 3, 4, mChFlag = False)
        ## @brief           Create a channel for motor Y.
        #  @details         Using the channel method within the motor driver,
        #                   the Y motor will be created using channels 1 and 2
        #                   to allow for the Y motor to have its own duty cycles.
        self.motY           = self.motorDriverObj.channel(Pin.cpu.B4, Pin.cpu.B5, 
                                                          Timer(3, freq=20000), 1, 2, mChFlag = False)
        
    @micropython.native    
    def motorTaskFcn(self):
        '''
        @brief      State machine for running the motor drivers.
        @details    This generator is used to run the motor task. The main states
                    are initializing the motors, sending them new duty cycles,
                    and allowing the user to restart the motors if they were to 
                    hit a fault or a stop command.
        '''
        ## @brief    Contains the information for which state its in 
        state        = 0 # start in the init state
        
        while True: 
            
            if state == self.S0_INIT:
                if self.motorDriverObj.fault:
                    self.motorDriverObj.disable()
                    state = self.transitionTo(state, self.S2_LATCH)
                else:
                    self.motorDriverObj.enable()
                    state = self.transitionTo(state,self.S1_MOVE_MOTORS)  
                
            elif state == self.S1_MOVE_MOTORS:
                if self.motorDriverObj.fault:
                    if self.mTaskFlag:
                        print('mTask: fault occured') 
                    self.motorDriverObj.disable()
                    shares.motFault.put(1) 
                    state = self.transitionTo(state, self.S2_LATCH)
                else:
                    self.motX.setLevel(shares.dutyX.get())
                    self.motY.setLevel(shares.dutyY.get())
                
            elif state == self.S2_LATCH:
                if not self.motorDriverObj.fault:
                    if self.mTaskFlag:
                        print('mTask: unlatched')
                    shares.motFault.put(0)
                    state = self.transitionTo(state,self.S0_INIT)
                    
            yield(state)
    
    @micropython.native
    def transitionTo(self, state, newState):      # state transition method
        '''
        @brief      Method for transitioning states
        @details    The method will reassign the `state` variable when
                    directed to transition.
        @param      state The current state of the system.
        @param      newState The new desired state for the system.
        @return     The new state of the finite state machine.            
        '''
        if self.mTaskFlag:
            print('mTask: S' + str(state) + '->' + 'S' + str(newState) )
        return newState               # now that transition has been declared, update state