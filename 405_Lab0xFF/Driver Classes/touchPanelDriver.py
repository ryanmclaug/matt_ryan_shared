'''
@file       touchPanelDriver.py
@brief      This class will find the location of the ball on the resistive pad.
@details    By interfacing with the resistive network on the resistive touch
            pad and analog-to-digital convertors, the ball's location (x and y) can be
            solved for. Additionally, the class can determine if the ball is on
            the platform based on the z direction. A calibration test has also
            been included to calibrate the touch pad for more accurate 
            position tracking abilities.
            <a class="custom" href="https://bitbucket.org/ryanmclaug/matt_ryan_shared/src/master/405_Lab0xFF/Driver%20Classes/touchPanelDriver.py" 
            target="_blank" rel="noopener noreferrer">Touch Panel Driver Source Code</a> 
@author     Matthew Frost
@author     Ryan McLaughlin
@date       Originally created on 05/13/21 \n Last modified on  05/22/21
'''
from pyb import Pin, ADC
import time
import micropython
from micropython import const
# from print_task import put as ptPut


class touchPanelDriver:
    ''' 
     @brief    Position finder for the ball.
     @details  This class will allow for the position and velocity of the ball
               to be calculated using the resisive touch pad.
    '''
    
    def __init__(self, debugFlag = True, xm = Pin.cpu.A1, xp = Pin.cpu.A7, 
                        ym = Pin.cpu.A0, yp = Pin.cpu.A6, boardLength = const(176),
                        boardWidth = const(107), centerXPos = const(88), 
                        centerYPos = const(53)):
        ''' 
        @brief    Create an encoder object to find position and speeds.
        @details  This object will be called in order to find the
                  current positions of the ball, or to calibrate the pad.
        @param    debugFlag Enables or prevents certain print statements from
                  appearing.
        @param    xm The pin associated with the negative x terminal of the 
                  resistive pad.
        @param    xp The pin associated with the positive x terminal of the 
                  resistive pad.
        @param    ym The pin associated with the negative y terminal of the 
                  resistive pad.
        @param    yp The pin associated with the positive y terminal of the 
                  resistive pad. 
        @param    boardLength The length of the long side of the board in mm
        @param    boardWidth The width of the short side of the board in mm
        @param    centerXPos The horizontal distance from the edge of the board
                  to the center, which should be about half the boardLength
        @param    centerYPos The vertical distance from the edge of the board
                  to the center, which should be about half the boardWidth     
        '''
        ## @brief       Debugging flag for detailed analysis while running
        self.debugFlag  = debugFlag  
        ## @brief        Pin object for the negative x direction circuit
        self.xm          = xm
        ## @brief        Pin object for the positive x direction circuit
        self.xp          = xp
        ## @brief        Pin object for the negative y direction circuit
        self.ym          = ym
        ## @brief        Pin object for the positive y direction circuit
        self.yp          = yp
        ## @brief        Length of the board (x direction) in mm
        self.boardLength = boardLength
        ## @brief        Length of the board (y direction) in mm
        self.boardWidth  = boardWidth
        ## @brief        Center coordinate (x direction) in mm
        self.centerXPos  = centerXPos
        ## @brief        Center coordinate (y direction) in mm
        self.centerYPos  = centerYPos
        
    @micropython.native   
    def allPositions(self):
        ''' 
        @brief    Optimally return the position of the ball in 3 coordinates.
        @details  This method gets the x and y positions of the ball as well as
                  the the 'z' coordinate, which will tell whether the platform
                  has something on it. The order is run by getting X, then Z, 
                  then Y in order to reduce the number of pin-switching. This 
                  should produce a fast enough response to accurately run the 
                  controller (under 500 microseconds).
        '''
        # GET X FAST
        
        xmPin = Pin(self.xm, Pin.OUT_PP, value = 0)
        
        xpPin = Pin(self.xp, Pin.OUT_PP, value = 1)
        
        ypPin = Pin(self.yp, Pin.IN)
        
        ymADC = ADC(Pin(self.ym, Pin.IN))

        # From Ryan Calibration Testing:
        # Averages from 4/5 succesful runs: mx = -0.04829105, xc = 2018.925, my = -0.03374071, yc = 1962.375
        
        XPos = -0.04913*(ymADC.read() - 2038.17) # Matthew
        # XPos = -0.04829*(ymADC.read() - 2018.93) # Ryan
        
        # GET Z FAST
        xpPin = Pin(self.xp, Pin.IN)
        ypPin = Pin(self.yp, Pin.OUT_PP, value = 1)
        
        ZPos = ymADC.read() < 3800
        
        # GET Y FAST
        ymPin = Pin(self.ym, Pin.OUT_PP, value = 0)
        
        xmADC = ADC(Pin(self.xm, Pin.IN))
        
        currentCoordinates = (XPos, -0.03382*(xmADC.read() - 1970.93), ZPos) # Matthew
        # currentCoordinates = (XPos, -0.03374*(xmADC.read() - 1962.38), ZPos) # Ryan
        
        return currentCoordinates
   

    # pre optimization methods    
    @micropython.native
    def getXPos(self): 
        '''
        @brief      Get the X position of the ball.
        @details    This method gets the x position of the ball from reading the
                    details given from the resistive touch panel.
        '''
        ## @brief  definition of pyb.Pin object for xm (x minus)
        self.xmPin = Pin(self.xm, Pin.OUT_PP, value = 0)
        ## @brief  definition of pyb.Pin object for xp (x plus)
        self.xpPin = Pin(self.xp, Pin.OUT_PP, value = 1)
        ## @brief  definition of pyb.Pin object for ym (y minus)
        self.ymPin = Pin(self.ym, Pin.IN)
        ## @brief  definition of pyb.Pin object for yp (y plus)
        self.ypPin = Pin(self.yp, Pin.IN)
        ## @brief Analog to digital converter for determining x and z positions
        self.ymADC = ADC(self.ymPin)
        ## @brief    x-position of the ball in millimeters
        self.XPos = self._mx_*(((1/3)*(self.ymADC.read() + self.ymADC.read() + self.ymADC.read())) - self._xc_)
        
        return self.XPos
    
    @micropython.native    
    def getYPos(self): 
        '''
        @brief      Get the Y position of the ball.
        @details    This method gets the y position of the ball from reading the
                    details given from the resistive touch panel.
        '''
        self.xmPin = Pin(self.xm, Pin.IN)
        self.xpPin = Pin(self.xp, Pin.IN)
        self.ymPin = Pin(self.ym, Pin.OUT_PP, value = 0)
        self.ypPin = Pin(self.yp, Pin.OUT_PP, value = 1)
        ## @brief Analog to digital converter for determining y position
        self.xmADC = ADC(self.xmPin)
        ## @brief    y-position of the ball in millimeters
        self.YPos = self._my_*(((1/3)*(self.xmADC.read() + self.xmADC.read() + self.xmADC.read())) - self._yc_)
        
        return self.YPos
    
    @micropython.native        
    def getZPos(self): 
        '''
        @brief      Use the Z position to determine if the ball is touching.
        @details    This method gets the z position of the ball from reading the
                    details given from the resistive touch panel. A "True" means
                    the ball is on the platform.
        '''
        self.xmPin = Pin(self.xm, Pin.OUT_PP, value = 0)
        self.xpPin = Pin(self.xp, Pin.IN)
        self.ymPin = Pin(self.ym, Pin.IN)
        self.ypPin = Pin(self.yp, Pin.OUT_PP, value = 1)

        self.ymADC = ADC(self.ymPin)
        ## @brief     z-position of the ball
        #  @details   `True` if the ball is on the platform, and `False` if 
        #             the ball is not on the platform
        self.ZPos = self.ymADC.read() < 3800
        
        return self.ZPos
    
    @micropython.native
    def calibration(self):
        '''
        @brief      Calibrate the board for position.
        @details    This method allows the user to calibrate the board since the
                    resistive touch panel is not perfect. The data will then be 
                    used to find the position of the ball for the rest of the
                    driver. This method is utilized by the touchPanelCalibration_main.py
                    file to run multiple tests and perform averaging to get better data.
        '''
        for i in range(2):
            if i == 0:
                print('PRESS THE BALL TO THE CORNER NEAR THE MOTORS')
            elif i == 1:
                print('PRESS THE BALL TO THE CENTER OF THE BOARD')
            
            print('3')
            time.sleep(1)
            print('2')
            time.sleep(1)
            print('1')
            time.sleep(1)
            print('Calibrating')
            
            time.sleep(5)
            
            # For getting x1 voltage
            self.xmPin = Pin(self.xm, Pin.OUT_PP, value = 0)
            self.xpPin = Pin(self.xp, Pin.OUT_PP, value = 1)
            self.ymPin = Pin(self.ym, Pin.IN)
            self.ypPin = Pin(self.yp, Pin.IN)
            
            self.ymADC = ADC(self.ymPin)
            
            calibXList = [0]*10
            for x in range(len(calibXList)):
                calibXList[x] = self.ymADC.read()

            # For getting y voltage
            self.xpPin = Pin(self.xp, Pin.IN)
            self.xmPin = Pin(self.xm, Pin.IN)
            self.ymPin = Pin(self.ym, Pin.OUT_PP, value = 0)
            self.ypPin = Pin(self.yp, Pin.OUT_PP, value = 1)
            
            self.xmADC =ADC(self.xmPin)
        
            calibYList = [0]*10
            for x in range(len(calibYList)):
                calibYList[x] = self.xmADC.read()
            
            if i == 0:
                calV1 = (sum(calibXList)/len(calibXList), sum(calibYList)/len(calibYList))
                print(calV1)
            else:
                calV2 = (sum(calibXList)/len(calibXList), sum(calibYList)/len(calibYList))
                print(calV2)

        self._mx = 0.5*self.boardLength/(calV1[0] - calV2[0]) # mm/count 
        print('xSlope: {:}'.format(self._mx))
        self._xc = calV2[0] # mm
        print('xIntercept: {:}'.format(self._xc))
                
        self._my = 0.5*self.boardWidth/(calV1[1] - calV2[1]) # mm/count
        print('ySlope: {:}'.format(self._my))
        self._yc = calV2[1] # mm
        print('yIntercept: {:}'.format(self._yc))
        
        return [self._mx, self._xc, self._my, self._yc]
                