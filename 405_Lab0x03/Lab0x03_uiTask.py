'''
@file       Lab0x03_uiTask.py
@brief      Class to run state machine on Nucleo and communicate with Nucleo.
@details    Interface with the Spyder Console window through the serial 
            communication. If a keyboard key has been pressed, it will be sent
            through serial to this script, where it will call for an action, 
            such as starting data collection via the ADC. Data will be sent back 
            through serial to output into the Spyder Console or to plot.
            *See source code here:* <a class="custom" href="https://bitbucket.org/ryanmclaug/matt_ryan_shared/src/master/405_Lab0x03/Lab0x03_uiTask.py" 
            target="_blank" rel="noopener noreferrer">uiTask Source Code</a>

@author     Matthew Frost
@author     Ryan McLaughlin
@date       Originally created on 04/29/21 \n Last modified on 05/02/21
'''
import pyb
from pyb import UART
from array import array

# turn off REPL for the UART
pyb.repl_uart(None) # disables repl on st-link usb port
## @brief Assign variable to UART port 2
myuart = UART(2)

# while True:
class uiTask:
    ''' 
     @brief    UI Task interfaces between front end and the serial port.
     @details  This class will take data and report it through serial to the 
               front end. It also receives characters/commands from the front 
               end.
    '''
    ## @brief           Basic init state
    S0_INIT             = 0   
    ## @brief           Wait for user to press a letter
    S1_READY_FOR_INPUT  = 1   
    ## @brief           Sample data and store to array
    S2_COLLECT_DATA     = 2 
    ## @brief           Print data back to PC
    S3_PRINT_DATA       = 3   
    ## @brief           Reset for another round of data collection
    S4_RESET            = 4   
    
    def __init__(self, debugFlag = True):
        ''' 
        @brief    Initialize the UI task.
        @details  This task will collect and send data to the front end for 
                  the voltage reading from the button circuitry.
        @param    debugFlag Enables or prevents certain print statements from
                  appearing.
       '''
        ## @brief            Contains the information for which state its in   
        self.state           = 0 # start in the init state
        ## @brief            Debugging flag for detailed analysis while running
        self.debugFlag       = debugFlag # used to view debugging print statements
        ## @brief            An array to store voltage data to plot
        self.voltages        = array('H', (0 for index in range (5000))) # set up array for function values
        ## @brief            Holds the input commands (letters) from serial 
        self.val             = 0 # set keyboard input variable to 0
        ## @brief            Counter used for sending data one element at a time
        self.n               = 0 # set counter = 0
        ## @brief            Sends a signal to start data sending to console
        self.sendingDataFlag = 1 # counter for sending the data
        ## @brief            Create an ADC on pin PA0
        self.adc             = pyb.ADC(pyb.Pin.board.PA0)  
        ## @brief            Create a timer running at 250kHz
        self.tim             = pyb.Timer(6, freq=250000) 
        
    def run(self): 
        '''
        @brief      Report necessary measurements the front end.
        @details    This method will wait for a 'g' to be pressed, and then 
                    subsequently take data until a button press has been 
                    identified. It will then send the data through serial to 
                    the front end for further data processing.
        '''
        if self.state == self.S0_INIT:
            print('UI: To start data collection, press g and then the button')
            self.transitionTo(self.S1_READY_FOR_INPUT)
            
        elif self.state == self.S1_READY_FOR_INPUT:
            
            if myuart.any():
                self.val = myuart.readchar()
                print('UI: You sent an ASCII '+ str(self.val) +' to the Nucleo.')
            
            # ASCII char number for a lowercase g
            if self.val == 103:
                print('UI: You have pressed g. Starting data collection now.')
                self.transitionTo(self.S2_COLLECT_DATA)
            else:
                pass
            self.val = None     # Reset self.val 
                    
        elif self.state == self.S2_COLLECT_DATA: 

            while True:
                
                # Fill data
                self.adc.read_timed(self.voltages, self.tim) 

                # Once data is all the way full, check different in voltage
                
                ## @brief Difference between final and initial voltage readings from ADC
                self.differenceInVoltage = self.voltages[-1] - self.voltages[0]
                
                if self.differenceInVoltage > 3900:
                    # If a button press has been detected, go forward with the data
                    print('UI: The button has been pressed and released, save data')
                    self.transitionTo(self.S3_PRINT_DATA)
                    break
                
                else:
                    # If no button press has been found, reset the array and try again
                    self.voltages = array('H', (0 for index in range (5000)))
            

        elif self.state == self.S3_PRINT_DATA:
            if self.debugFlag == True:
                print('UI: sendingDataFlag: ' + str(self.sendingDataFlag))
                
            if self.debugFlag == True:
                print('UI: printing data now.')
                print('UI: self.n is: ' + str(self.n))
                
            if self.n <= len(self.voltages)-1:
                if self.debugFlag == True:
                    print('UI: Writing: {:}, {:} \r\n'.format(self.n*4,self.voltages[self.n]))
                myuart.write('{:}, {:} \r\n'.format(self.n*4,self.voltages[self.n]))
                self.n +=1
                
            else: 
                myuart.write('plot\r\n')
                self.transitionTo(self.S4_RESET) 
                
            
        elif self.state == self.S4_RESET:
                self.transitionTo(self.S4_SEND_PLOT_CMD) 
                
        elif self.state == self.S4_SEND_PLOT_CMD:
            # tell the front end to plot
            myuart.write('plot\r\n')
            self.transitionTo(self.S5_FINISHED)
            
        elif self.state == self.S5_FINISHED:
            # Reset the arrays and restart for more data collection
            self.voltages = array('H', (0 for index in range (5000)))
            self.n = 0
            self.transitionTo(self.S0_INIT)
            
            
    
    def transitionTo(self, newState):      # state transition method
        '''
        @brief      Method for transitioning states
        @details    The method will reassign the `self.state` variable when
                    directed to transition.
        @param      newState Controls which state is the new state for the FSM.
        '''
        if self.debugFlag == True:
            print('UI: Transitioning from ' + 'S' + str(self.state) + '->' + 'S' + str(newState) )
        self.state = newState # now that transition has been declared, update state