'''
@file       morseCode.py
@brief      Morse code class
@details    detailed description here
            
            \n *See source code here:* <insert link> \n
@image      html filename.jpg “Caption” width = xx% 
@author     Ryan McLaughlin
@author     Matt Frost
@date       Originally created on 02/06/21 \n Last modified on 02/07/21
'''

import random
import utime

class morseCode:
    
    # define dictionary
    # . = dot, - = dash, _ = space between parts of same letter, / = space between letters (added in below)
    # when concatenation occurs
    
    CODE = {'A': '._-',       'B': '-_._._.',   'C': '-_._-_.', 
            'D': '-_._.',     'E': '.',        'F': '._._-_.',
            'G': '-_-_.',     'H': '._._._.',   'I': '._.',
            'J': '._-_-_-',   'K': '-_._-',     'L': '._-_._.',
            'M': '-_-',       'N': '-_.',       'O': '-_-_-',
            'P': '._-_-_.',   'Q': '-_-_._-',   'R': '._-_.',
            'S': '._._.',     'T': '-',         'U': '._._-',
            'V': '._._._-',   'W': '._-_-',     'X': '-_._._-',
            'Y': '-_._-_-',   'Z': '-_-_._.'
        }
    
    # constructor
    def __init__(self, setLength = 5):
                 

        
        self.setLength = setLength        # how many letters per pattern
    
    
    # main method
    def morseGenerator(self, currentSet, newSet, setLength):
        
        # check if need to generate new pattern, or get next value in existing pattern
        if newSet == True:
            t0 = utime.ticks_us()
            # in the case of a new set, need a new pattern of letters
            n = 1
            self.pattern = []
            for n in range(setLength):
                self.pattern.append( random.choice( list(self.CODE.keys())) )       # append next letter to pattern
                print('letters chosen are ' + str(self.pattern) )
                
            print('time to generate pattern is ' + str( utime.ticks_diff( utime.ticks_us() , t0 ) ) + ' μs')
        
        # need as many letters as the set number is currently at
        self.thisPattern = self.pattern[0:currentSet]   
        
        # take string of letters, turn into morse code
        self.morse = '/'.join(self.CODE.get(i.upper()) for i in self.thisPattern)        
        print(self.morse)
        
        return(self.morse)
    
