'''
@file       motorDriver.py
@brief      Class for the motor driver shared by both motors
@details    This class contains methods and attributes common to both motors, such as
            enabling, disabling, and detecting faults. Furthermore, this class is
            capable of generating motor instances given pin and channel inputs. By
            having two seperate drivers, one for the motor driver as a whole and one
            for individual motor channels, clear seperation is made between the two,
            with the parent motor driver still having access to methods of each channel.
            For example, in the `disable()` method, the nSLEEP pin is set low, but each
            motor is also set to a duty cycle of zero.
            
            *See source code here:* 
            <a class="custom" href="https://bitbucket.org/ryanmclaug/matt_ryan_shared/src/master/405_Lab0xFF/Driver%20Classes/motorDriver.py" 
            target="_blank" rel="noopener noreferrer">Motor Driver Source Code</a>
@author     Matthew Frost
@author     Ryan McLaughlin
@date       Originally created on 05/20/21 \n Last modified on 06/03/21
'''
from pyb import Pin, ExtInt
from motorDriverChannel import motorDriverChannel
import micropython

class motorDriver:
    
    
    def __init__ (self, nSLEEP_pin, nFAULT_pin, mDriverFlag = True):
        ''' 
        @brief   Creates a motor driver by initializing GPIO pins.
        @details The user will have to input the pins for the motor and the 
                 potential timers and channels as specified in the 
                 manufacturers specifications for the specific motor.
                 Since both motors are connected to one motor driver, the motor driver
                 serves as a parent to the two motors, containing any details common
                 to both motors.\n
                 Example:\n
                 `self.motorDriverObj = motorDriver(Pin.cpu.A15 , Pin.cpu.B2, driverFlag = True)`
                 
        @param   nSLEEP_pin A pyb.Pin object to use as the enable/disable pin.
        @param   nFAULT_pin A pyb.Pin object used to detect faults such as high current, 
                 a stalled shaft, or any other unwanted condition.
        @param   mDriverFlag Enables or prevents certain print statements from
                 appearing.
        '''
        ## @brief       Debugging flag for detailed analysis while running
        self.mDriverFlag  = mDriverFlag 
        
        ## @brief       Used to enable or disable the motor
        self.nSLEEP_pin = Pin(nSLEEP_pin, mode=Pin.OUT_PP, value=1)
        
        ## @brief    Interupt triggered by fault detection in the motor driver
        #  @details  Upon any fault in the combined motor driver (falling edge on PB2), a callback
        #            is triggered, and `self.fault` is set to `True`. This variable is a flag for program
        #            operation to halt, and can only start back up once the fault has been cleared.
        self.nFAULT_pin = ExtInt(nFAULT_pin, mode = ExtInt.IRQ_FALLING, pull=Pin.PULL_UP, callback = self.faultCB)
        
        ## @brief      Pin attribute for blue user button 
        #  @details    The blue button on the board has been used to clear faults caught
        #              by the nFAULT pin. Namely, the callback function `clearFault()` is called.
        self.ButtonInt = ExtInt( Pin (Pin.cpu.C13)  , mode=ExtInt.IRQ_FALLING, pull=Pin.PULL_NONE, callback= self.clearFault)
        
        ## @brief     List containing copies of each motor channel instantiated
        #  @details   In order to keep "copies" of each motor channel created, the function 
        #             `channel()` appends to `self.motorList` the channel object being returned.
        #             With these copies, the more broad motor driver can still use the methods of
        #             each motor channel.
        self.motorList = []
        
        ## @brief     Boolean variable for fault detection
        #  @details   Initially set to `False` and set to `True` on a falling edge of the nFault pin
        self.fault = False
        
        ## @brief       Boolean variable to ignore unwanted faults
        #  @details     For reasons not yet fully understood, when a fault is cleared, the enable
        #               action triggers a fault. However, this is a false alarm, so it is desired
        #               to use logic to skip this fault. Set to 'True` only when `clearFault()` is
        #               called, and set back to false upon the false fault callback.
        self.ignoreFault = False
       
    @micropython.native    
    def enable (self):
        '''
        @brief      Enable the motor to allow for PWM to actuate the motor.
        @details    Sets the nSLEEP pin high, allowing all other pins related
                    to the motor to function
        '''
        if self.mDriverFlag:
            print('mDr: Enabling')
        self.nSLEEP_pin.high()
    
    @micropython.native    
    def disable (self):
        '''
        @brief      Disable the motor to prevent the PWM to actuate the motor.
        @details    Sets the nSLEEP pin low, as well as setting each motor duty
                    cycle to zero.
        '''
        if self.mDriverFlag:
            print('mDr: DISABLING')
        self.nSLEEP_pin.low()
        
        for motor in self.motorList:
            motor.setLevel(0)

    @micropython.native
    def faultCB (self, IRQ_src):
        '''
        @brief      Callback method for fault detection
        @details    When this callback is triggered by a falling edge on PB2 (connected 
                    to the nFAULT pin), `self.fault` is set to True (with the exception
                    of the "false alarm" case discussed in documentation for `self.ignoreFault`.
        @param      IRQ_src interupt request 
        '''
        if self.ignoreFault:
            if self.mDriverFlag:
                print('mDr: IGNORE FAULT')
            self.ignoreFault = False

        else:
            print('Fault detected, press the blue user button to clear.\n')
            self.fault = True

    @micropython.native
    def clearFault(self, IRQ_src):
        '''
        @brief      Callback method for clearing a fault
        @details    When this callback is triggered by a falling edge on PC13 (connected 
                    to the blue user button), two boolean variables are changed. First,
                    `self.fault` is set back to False to allow program operation to continue.
                    Secondly, `self.ignoreFault` is set to True in order to skip the next fault
                    incorrectly detected by the nFAULT pin.
        @param      IRQ_src interupt request 
        '''
        
        print('Fault cleared\n')
        self.fault = False
        self.ignoreFault = True

    @micropython.native
    def channel (self, IN1_pin, IN2_pin, IN_timer, ch1, ch2, mChFlag = False):
        '''
        @brief      Method for generation of motor channel objects
        @details    To allow for the motor driver parent to have access to methods of 
                    each individual channel, a method within the motor driver class
                    is responsible for instantiating motor objects. Each object is then "copied"
                    into a list within this class, such that it can be refrenced by other methods.\n
                    Example:\n
                    `self.motX = self.motorDriver.channel( Pin.cpu.B0, Pin.cpu.B1,
                    Timer(3, freq=20000), 3, 4, chFlag = True)`
                    
        @param      IN1_pin     First pin used for PWM
        @param      IN2_pin     Second pin used for PWM
        @param      IN_timer    Shared timer object for both PWM pins, containing timer
                                number and frequency information
        @param      ch1         First channel of timer
        @param      ch2         Second channel of timer
        @param      mChFlag     Debugging flag for motor channel print statements
        '''
        self.motorList.append(motorDriverChannel(IN1_pin, IN2_pin, IN_timer, ch1, ch2, mChFlag = False))
        return self.motorList[-1]
    

    


