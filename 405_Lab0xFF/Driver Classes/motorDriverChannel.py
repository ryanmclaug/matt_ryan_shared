'''
@file       motorDriverChannel.py
@brief      This driver will act as the channel and duty cycle assignment for the motors.
@details    This driver is responsible for individual motor operation, namely
            setting the PWM duty cycle. Each motor object definition is based on
            a timer object and two pins configured for PWM. 
            
            *See source code here:* 
            <a class="custom" href="https://bitbucket.org/ryanmclaug/matt_ryan_shared/src/master/405_Lab0xFF/Driver%20Classes/motorDriverChannel.py" 
            target="_blank" rel="noopener noreferrer">Motor Channel Driver Source Code</a> 
@author     Matthew Frost
@author     Ryan McLaughlin
@date       Originally created on 05/20/21 \n Last modified on 06/03/21
'''
from pyb import Timer
import micropython

class motorDriverChannel:
    
    def __init__ (self, IN1_pin, IN2_pin, IN_timer, ch1, ch2, mChFlag = False):
        ''' 
        @brief   Creates a motor driver channel object by initializing GPIO pins.
        @details Example:\n 
                 `self.motX = self.motorDriverObj.channel(Pin.cpu.B0, Pin.cpu.B1, Timer(3, freq=20000), 3, 4, mChFlag = False)`
        
        @param   IN1_pin    First PWM pin object 
        @param   IN2_pin    Second PWM pin object
        @param   IN_timer   A pyb.Timer object with correct timer number and frequency
        @param   ch1        First timer channel number
        @param   ch2        Second timer channel number
        @param   mChFlag    Debugging flag
        '''
        ## @brief       The timer for the motors
        #  @details     This is passed in with the specified timer number and
        #               frequency
        self.IN_timer   = IN_timer
        ## @brief       The timer channel object for half bridge 1 of the motor
        #  @details     This timer channel will be set to PWM in order to control
        #               the quadrature encoders at a specified channel number
        self.IN1_pin    = self.IN_timer.channel(ch1, mode=Timer.PWM, pin=IN1_pin)
        ## @brief       The timer channel object for half bridge 2 of the motor
        #  @details     This timer channel will be set to PWM in order to control
        #               the quadrature encoders at a specified channel number
        self.IN2_pin    = self.IN_timer.channel(ch2, mode=Timer.PWM, pin=IN2_pin)
        ## @brief       Debugging flag for detailed analysis while running
        self.mChFlag    = mChFlag
    
    @micropython.native
    def setLevel (self, level):
        ''' 
        @brief    Set the level of the motors.
        @details  This method sets the duty cycle to be sent to the motor to the 
                  given level. Positive values cause effort in one direction, negative 
                  values in the opposite direction.
        @param    level A signed integer holding the new duty cycle of the PWM signal 
                  sent to the motor. 
        '''
        # If the incoming level is greater than 100 or less than -100, 
        # auto-set the level to 100 or -100, respectively, since it's not 
        # possible to be larger than 100%
        if level > 100:
            level = 100
            if self.mChFlag: 
                print('mDrCh: Invalid duty, d = 100')
        
        elif level < -100:
            level = -100
            if self.mChFlag: 
                print('mDrCh: Invalid duty, d = -100')
            
        # Set the level cycle of the motor to the appropriate direction and value
        if level >= 0:
            self.IN1_pin.pulse_width_percent(level)
            self.IN2_pin.pulse_width_percent(0)
        
        else:
            self.IN1_pin.pulse_width_percent(0)
            self.IN2_pin.pulse_width_percent(-level)
