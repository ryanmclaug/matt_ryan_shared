# Made changes to the following files:
#
#  Lab0x02_A.py
#  Lab0x03_PC_Frontend.py
#  game.py (305)
#  cotask.py
#
##############################################################################
# FINAL TASKS
#  - Videos (testing)                          [ Matthew ]
#  - Review hw assignment documentation pages
#  - File documentation             ( double check )
#  - Report portion of documentation           [ controller - ryan ]
#                                              [ final 3 pages - matthew ]
#
#######################################################################################
#  ADDING EMBEDDED YOUTUBE VIDEOS
#   Steps:
#   1) Go to the youtube video, click the "share" button, then the "embed" option, and "copy"
#   2) Paste that text, and surround it with the following additional text:
#   3) Before the copied lines, put       \htmlonly <div style="text-align:center;">
#   4) After the copied lines,            </div> \endhtmlonly
#
#   Full example of centered video: 
#
# \htmlonly <div style="text-align:center;">
#<iframe width="560" height="315" src="https://www.youtube.com/embed/Y5xGzvfFYX4" 
#title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; 
#clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
# </div>\endhtmlonly
#
#######################################################################################
#
# 
# IMPORTANT REMINDERS
#   - check if data is being lost (make sure overwrite is on)
#   - if the ball is not on, don't send data
#   - if we are trying to put() a value in a SHARE, and there's already a value
#     in it, does it overwrite, or get stuck??
# 
# 
# THINGS TO CHANGE/ADD IN THE FUTURE
#   - float to int
#   - UART vs VCP
#   - real time plotting in front end
#   - use encoderDriver.update() function instead of getPosition() and getSpeed()
#   - move if self.motorDriverObj.fault: stuff in motorTask.py outside FSM
# 
# 
# FROM OH 06/01/21
#   - send less data?
#   - more duty cycle for pushing vs pulling? (controller task implementation)
#   - if not empty/if not full (logic for queues)
# 
# 
# FROM OH 06/02/2021
#   - alpha beta tuning? Try outputting the values. Larger B = more damping?
#     Look on wikipedia page for alpha beta stuff
#   - LAG!! late too
#     sum of (avg time/period) < 1 or late will occur
#   - period logic
#   - alpha beta messed up velocity a ton
#   - dataTask
# 
