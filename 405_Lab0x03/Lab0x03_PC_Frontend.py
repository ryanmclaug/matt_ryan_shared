'''
@file       Lab0x03_PC_Frontend.py
@brief      Set up UI between Spyder and Nucleo to collect and plot data.
@details    Use the serial port to send and receive data between Spyder console
            and Nucleo. Nucleo, when told to, will take data of the voltage
            corresponding to the voltage in the capacitor when the button 
            is released. This should go from having a voltage of 0V to a voltage
            equal to the supplied voltage (in our case, 3.3V). When the Python
            script recognizes that a button has been pressed, it will save the
            data and find the time constant of the capacitor. This is done by 
            post processing the data and allow the user to specify a range in
            which to compute the time constant.
            *See source code here:* https://bitbucket.org/ryanmclaug/matt_ryan_shared/src/master/Lab0x03/Lab0x03_PC_Frontend.py
@author     Ryan McLaughlin
@author     Matthew Frost
@date       Originally created on 04/29/21 \n Last modified on 05/03/21
'''
import serial
import csv
import keyboard
import os 
from matplotlib import pyplot as plt
from matplotlib import rcParams
import numpy as np
from math import floor

## @brief Set up serial communication through serial command
ser = serial.Serial(port='COM3', baudrate=115200, timeout=1)
ser.flushInput()

def sendChar(last_key):
    ''' 
    @brief   Sends a string through serial to Nucleo.
    @details When keyboard is triggered, encode that letter and send it through 
             serial to the Nucleo to read.
    @param   last_key The current key that was pressed.     
    '''
    ser.write(str(last_key).encode('ascii'))
    # print('FrontEnd: You have sent a letter')

def kb_cb(key):
    ''' 
    @brief   Callback function inside keyboard module.
    @details When keyboard is pressed, it automatically gets sent to the
             callback function to assign a variable the letter pressed. 
    @param   key The key that was pressed to trigger the callback function.
    '''
    global last_key
    last_key = key.name
    
def writeToCSV(data, filename):
    ''' 
    @brief   Strip and split the data into a csv file from the Nucleo.
    @details After data collection is finished, the data must be stripped
             and split in order to write to a csv file.
    @param   data The data from the serial port that needs to be saved into the CSV.
    @param   filename The specific file to save the data to.                 
    '''
    dataStripped = data.strip()
    # print('Front end: dataStripped is currently: ' + str(dataStripped))
    dataSplit    = dataStripped.split(', ')
    
    timeData  = float(dataSplit[0])
    # print('Front end: current timeData is: ' + str(timeData))
    voltageData = float(dataSplit[1])
               
    with open(filename,"a") as f:
        writer = csv.writer(f,delimiter=",")
        writer.writerow([timeData, voltageData])       

def clearFiles(fileNames):
    '''
    @brief      Method for deleting files
    @details    Since the csv writer is set to append to a certain file, the file to be written to must be removed
                each time the PC collects new data. If it was desired, this could be replaced by a method to rename
                the next file to write to a new file each time. For this, the `os` module is used.
    @param      fileNames is a list of strings containing the various files written each time the frontEnd is run 
    '''    
    _filesRemoved = 0
    
    for n in range(len(fileNames)):
        
        _fileName = fileNames[n]
        
    # check if file exists 
        if os.path.exists(_fileName):
            os.remove(_fileName)
            _filesRemoved += 1
            
    print('{:} .csv/.png files have been removed'.format(_filesRemoved))
    
def processData(originalCSV,newCSV):
    '''
    @brief      Method for post processing the data
    @details    Since the button was pressed over a range of 5000ms, but the
                data applicable is only about 2ms. Thus, the data has to be 
                shortened to only display the relevant data.
    @param      originalCSV The CSV file that contains the raw data.
    @param      newCSV The new CSV file that contains the shortened data.
    '''    
    # load in data from csv
    time, voltage = np.loadtxt(originalCSV, delimiter=',', usecols=(0, 1), unpack=True)
    startIndex = 0
    endIndex = 0
    processingBool = False
    
    for idx, voltageValue in enumerate(voltage):
        # get starting index
        if startIndex == 0:
            if voltageValue > 100:
                startIndex = idx - 50
                
        if startIndex != 0:
            if voltageValue > 4070:
                endIndex = idx + 100
                break  # don't need to keep running algorithm
    
    if startIndex <= 0 or endIndex > len(time)-1:
        print('Data from button press is not sufficient to form a full response plot.')
        
    else:   # write modified data to a new csv
        processingBool = True    
        
        for timeValue, voltageValue in zip(time[startIndex:endIndex], voltage[startIndex:endIndex]):
            
            with open(newCSV,"a") as f:
                writer = csv.writer(f,delimiter=",")
                writer.writerow([timeValue, voltageValue])
            
    return processingBool
    
    
def getTimeConstant(csvInput,pngLinearization,pngFinal):
    '''
    @brief      Method for getting the time constant.
    @details    This method will display the plot for the user, where they can 
                specify the range of time that has a good linear relation to 
                it. This will then plot a line-of-best-fit to the data, where
                the inverse of the slope is the time constant. This method is 
                much more accurate for numerical data than using the 63.2% of 
                the data method, or finding the tangent line at the start of the
                data dn finding the time it intersects the steady state value.
    @param      csvInput Input csv to plot data from
    @param      pngLinearization File name to save the log-axes plot to a png.
    @param      pngFinal File name for the final plot of voltage time-response.
    '''    
    # import data
    x, count = np.loadtxt(csvInput, delimiter=',', usecols=(0, 1), unpack=True)
    voltage = (count/4095)*3.3
    y = -np.log(max(voltage)+.00001-voltage)     # log axes to get linear relationship
    
    # plot initial data for user to view
    rcParams['font.family'] = 'monospace'
    fig1 = plt.figure( 1,figsize=(6, 6), dpi=200)   # create a figure
    ax1 = fig1.add_subplot(1,1,1)                   # create axes for adding to plot
    plt.scatter(x, y, color='black',  s=2)          # plot data as scatter
    plt.xlabel(r'Time, $t$ [ ${\mu}$s ]')           # x label
    plt.ylabel(r'$-ln(V_{dd} - V_{c})$')            # y label
    plt.title('Time Constant Determination Plot')   # title
    plt.axis([min(x),max(x),min(y),max(y)])         # axes

    plt.show()                                      # display plot
    
    print('You will now be prompted to choose a window for determining the time ' 
          'constant. Please open the Plots pane to view the current plot.')
    
    # determine closest time value to user input
    x1 = floor(int(input('Enter the desired window start time: '))/4)*4
    x2 = floor(int(input('Enter the desired window end time: '))/4)*4
    
    # use np.where to find indices of time values for window
    idx1 = np.where(x == x1)[0][0]
    idx2 = np.where(x == x2)[0][0]
    
    # linear fit to window specified
    m, b = np.polyfit(x[idx1:idx2], y[idx1:idx2], 1)    #  m=slope, b=intercept
    
    # convert m to time constant
    tau = (1/m)
    error = (tau-470)/470
    
    # append to existing figure
    ax1.plot(x[idx1:idx2], m*x[idx1:idx2] + b, color = 'red')
    fig1.savefig(pngLinearization)
    
    fig2 = plt.figure(figsize=(6, 6), dpi=200)   # create a figure
    plt.plot(x-min(x), voltage, color='black')
    plt.xlabel(r'Time, $t$ [ ${\mu}$s ]')
    plt.ylabel(r'Voltage, $v$ [ V ]')
    plt.title('User Button Step Response')
    plt.axis([0,max(x)-min(x),0,3.5])
    myString = str(r'$\tau_{theoretical}$   = 470.0 $\mu$s'+'\n'+
                   r'$\tau_{experimental}$ = '+'${:.1f}$ $\mu$s\n'.format(tau)+
                   r'% error   = {:.2%}'.format(error))
    plt.text( 2000, 1.5,myString)
    fig2.savefig(pngFinal)
    
    return tau  
    
## @brief keeps track of if the intro message should be displayed
introMessage = True

## @brief holds the value of the current keyboard letter
last_key = None

## @brief    File name for initial plot
pngInitial   = "Lab0x03_allData.png"
## @brief    File name for data plotted on log axes
pngLog       = "Lab0x03_logData.png"
## @brief    File name for final plot
pngModified  = "Lab0x03_finalPlot.png"


## @brief    File name of the exported data
csvInitial   = "Lab0x03_allData.csv"
## @brief    CSV that holds the shortened data
csvModified  = "Lab0x03_modifiedData.csv"

# check to see if the csv file exists, and if it does, delete it so it can be
# written over with new data
clearFiles([pngInitial,pngLog,pngModified,csvInitial,csvModified])

## @brief       Debugging flag to hide certain print statements
debugFlag       = False

# Predefine a list of keys that the keyboard will respond to.
## @brief  list the specific keys used in the front end
callbackKeys       = ["g"] 
    
for _thisKey in callbackKeys:  
        keyboard.on_release_key(_thisKey, callback = kb_cb)

## @brief      The outputted data from the serial port
dataFromNucleo = None

while True:
    try:      
        
        if introMessage == True:
                
            print('Press g to start data collection, then press the blue '
                  'user button on the Nucleo board until a step response '
                  'has been properly captured.')
                
            introMessage = False
        
        ## @brief  Most recent data from serial communciation between PC and Nucleo
        dataFromNucleo = ser.readline().decode('ascii')    # read from uart

        
        if dataFromNucleo:
            if debugFlag == True:
                print('dataFromNucleo is not empty')
            
            ## @brief Non empty line from Nucleo serial communication
            currentFromNucleo = dataFromNucleo
            
            
            if currentFromNucleo == 'plot\r\n':     
                keyboard.unhook_all()       # allow user to access all keyboard keys (for inputs)
                
                print('Processing data')
                ## @brief  Flag to show that the data could be successfully processed.
                succesfulProcessing = processData(csvInitial,csvModified)
                
                # after data is processed, call getTimeConstant()
                if succesfulProcessing == True:
                    print('Data was succesfully processed')
                else:
                    print('Failed to process data, please try again.')
                    break
                
                ## @brief The time constant gathered from the button's response.
                currentTau = getTimeConstant(csvModified,pngLog,pngModified)
                print('Processing is complete, the final step response can be viewed '
                      'from the Plots pane.')
                break
            
                
            elif currentFromNucleo[0:1] == '0,':        # aka the first line in the csv file
               print('Data collection is done, starting to write to CSV') 
               writeToCSV(currentFromNucleo,csvInitial)
               
               
            else:   # this case is for writing to csv file
                if debugFlag == True:
                    print('writing ' + currentFromNucleo + ' to csv file')
        
                writeToCSV(currentFromNucleo,csvInitial)
                
        if last_key is not None:    # if a key is pressed, transition to new state
            
            sendChar(last_key)    
            # print('You pressed ' + last_key)
            last_key = None     # reset last_key


    except KeyboardInterrupt:
        keyboard.unhook_all()
        break

# Turn off the callbacks so next time we run this behave as expected
keyboard.unhook_all()

# closer serial port
ser.close()