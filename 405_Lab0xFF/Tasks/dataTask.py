'''
@file       dataTask.py
@brief      Task for data collection and serial communication with PC Frontend.
@details    This task will send data to the PC Frontend once the program has 
            stopped. Due to limited memory on the Nucleo board, only the 5 sets
            of data can be sent: time, position/velocity (in one direction), and
            tilt angle/angular velocity (in one direction). The program will 
            therefore need to be rerun in order to get data in both directions.
            While this is not ideal, it is the reality of the hardware memory.
            \n *See source code here:*  \n
            <a class="custom" href="https://bitbucket.org/ryanmclaug/matt_ryan_shared/src/master/405_Lab0xFF/Driver%20Classes/dataTask.py" 
            target="_blank" rel="noopener noreferrer">Data Collection Task Source Code</a> 
@author     Matthew Frost
@author     Ryan McLaughlin
@date       Originally created on 05/27/21 \n Last modified on 06/03/21
'''

import shares
from pyb import UART
import pyb
import micropython 

# turn off REPL for the UART
pyb.repl_uart(None) # disables repl on st-link usb port
## @brief Assign variable to UART port 2
myuart = UART(2)

class dataTask:
    ''' 
     @brief    Data collection system for the controller.
     @details  This class collects the data from shares.py and send it through
               serial to the PC Frontend to plot it.
    '''
    def __init__(self, debugFlag = True):
        ''' 
        @brief    Create a data collection task.
        @details  This task will be called at the end of the program in order
                  send the data through the serial port.
        @param    debugFlag Enables or prevents certain print statements from
                  appearing.
        '''
        ## @brief        Debugging flag for detailed analysis while running
        self.debugFlag   = debugFlag # used to view debugging print statements
        ## @brief        A flag to determine if we're saving x or y data
        self.dataToSave  = shares.dataToSave
        
    @micropython.native    
    def dataTaskFcn(self):
        '''
        @brief      Send the data over serial to the PC Front End.
        @details    When the user taps CTRL-C, this file is run in order to
                    send the data over serial, one line at a time. Once all the
                    data has been sent, a plotting command will be sent for the
                    PC FrontEnd to generate plots.
        '''
        while shares.time.any():
            # send the next row of data to the PC Frontend
            if self.dataToSave == 'x':
                myuart.write('{:}, {:.2f}, {:.2f}, {:.2f}, {:.2f} \r\n'.format(shares.time.get(), 
                                                                               shares.xData.get(), 
                                                                               shares.xDotData.get(), 
                                                                               shares.thYData.get(),
                                                                               shares.thDotYData.get()))
            else:
                myuart.write('{:}, {:.2f}, {:.2f}, {:.2f}, {:.2f} \r\n'.format(shares.time.get(), 
                                                                                shares.yData.get(),
                                                                                shares.yDotData.get(),
                                                                                shares.thXData.get(),
                                                                                shares.thDotXData.get()))
        myuart.write('plot\r\n')