'''
@file       main.py
@brief      Create an object for the uiTask to be run
@details    This is named main.py so that it automatically runs when the Nucleo
            is reset. All it does is create and run an instance of te uiTask
            *See source code here:* https://bitbucket.org/ryanmclaug/matt_ryan_shared/src/master/Lab0x03/main.py
@author     Matthew Frost 
@author     Ryan McLaughlin
@date       Originally created on 04/29/21 \n Last modified on 05/03/21
'''

from Lab0x03_uiTask import uiTask

if __name__ == "__main__":
    
    ## @brief An instance of the Ui Task
    task1 = uiTask(False) 
  
    while True: 
        try: 
            # Run the tasks continuously
            task1.run() 
            
        except KeyboardInterrupt:
            print('Ctrl-C has been pressed, exiting program.')
            # This except block catches "Ctrl-C" from the keyboard
            # to end the while(True) loop while desired
            break
    
    # Program de-initialization
    print('Thanks for testing out the program. See you next time!')
