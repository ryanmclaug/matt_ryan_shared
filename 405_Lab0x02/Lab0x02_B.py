'''
@file       Lab0x02_B.py
@brief      Use timer interrupt functions to measure reaction times. 
@details    Using the Nucleo L476RG board, two timers will be configured in an 
            Output Compare (OC) and Input Capture (IC) mode in order to measure  
            the reaction time of a user pressing the B1 button when the LD2 
            LED turns on. The LED will be controlled with the OC timer, where 
            it will be off for a random value between 2-3 seconds, turn on for
            exactly 1 second, and repear. The IC timer will be triggered when
            the B1 button is pressed on a falling edge. The difference in these
            two regions will be used to calculate a reaction time.
            \n *See source code here:* https://bitbucket.org/ryanmclaug/matt_ryan_shared/src/master/405_Lab0x02/Lab0x02_B.py \n
@image      html Lab0x02B_MainFSM.jpg " " width = 30%
@details    \n <CENTER> **Figure 1.** Overall program FSM. </CENTER> \n
@image      html OC_FSM.jpg " " width = 35%
@details    \n <CENTER> **Figure 2.** FSM Output Compare Interrupt. </CENTER> \n
@author     Ryan McLaughlin
@author     Matthew Frost
@date       Originally created on 04/26/21  \n Last modified on 04/27/21 
'''


import random
import pyb
import micropython
micropython.alloc_emergency_exception_buf(100) # for more readable error messages

def OCInterrupt(tim2): # Output Compare (OC) interupt method
        '''
        @brief      Callback function for the OC Interrupt.
        @details    Timer 2, Channel 1 is configured in an output compare 
                    method for timing the LED. The channel is configured with 
                    the OC_TOGGLE mode, where the pin for the LED is toggled
                    on and off to control the light emitted. Within this 
                    callback, the compare value is reset to account for the 
                    next callback. This alternates between staying off for 2-3
                    seconds and staying on for exactly 1 second.
        '''
        # In order to utilize the callback and save variable information,
        # a few of the important variables are defined as global.
        global newCompareVal, lastCompareVal, stateOC

        # Brief finite state machine for the OC control.
        S0_LED_WAS_OFF = 0   # state to show the led was off but turning on
        S1_LED_WAS_ON  = 1   # state to show the led was on but turning off
        
        if stateOC == S0_LED_WAS_OFF:
            lastCompareVal = newCompareVal  # lastCompareVal now holds the 
                                            # start time to when the LED was 
                                            # turned on
            newCompareVal += 1000000        # newCompareVal now holds the end 
                                            # time for when the LED should 
                                            # be turned off
            stateOC = S1_LED_WAS_ON         # Transition to the next state
            
        elif stateOC == S1_LED_WAS_ON:
            lastCompareVal = newCompareVal  # lastCompareVal now holds the 
                                            # start time to when the LED was 
                                            # turned off
            newCompareVal += random.randrange(2000000,3000000) # newCompareVal now holds the end time
                                                               # for when the LED should be turned off
            stateOC = S0_LED_WAS_OFF        # Transition to the previous state
        
        t2ch1.compare(newCompareVal) # reset counter for the compare method to new value
    
def ICInterrupt(tim2): # Input Capture (IC) interupt method
        '''
        @brief      Callback function for the IC Interrupt.
        @details    The B1 button is hardwired to pin PB3, which is controlled
                    by Timer 2, Channel 2. This is configured as an IC timer,
                    with a polarity on the falling edge. When the button is 
                    pressed, the timerchannel.capture() feature is used to get
                    the current count of the system.
        '''
        # In order to utilize the callback and save variable information,
        # a few of the important variables are defined as global.
        global buttonPressCount, buttonPress
        
        # buttonPres is set to true when the falling edge is detected
        if pinA5.value() == 1:
            # print('LED is on, user pressed')
            buttonPress = True
            # Upon interrupt, the current count of the timer channel will be 
            # read and stored in the variable buttonPressCount
            buttonPressCount = t2ch2.capture()

        
    
if __name__ == "__main__":

    ## @brief    Pin hardwired to the B1 USER button.
    #  @details  The PB3 pin will be used in order to control the IC interrupt
    #            from an external press of the B1 USER button on the Nucleo.
    pinPB3 = pyb.Pin (pyb.Pin.cpu.B3) 
    
    ## @brief    Pin attribute for LED. 
    #  @details  The A5 pin will be used to control the OC interrupt callback
    #            for the LED.
    pinA5 = pyb.Pin (pyb.Pin.board.PA5, mode = pyb.Pin.OUT_PP) 
    
    ## @brief    32-bit timer used to control the IC and OC interrupts. 
    #  @details  Timer 2 will be used for its 32-bit counter for high
    #            definition with a period of 0x7FFFFFF and prescaler = 79.
    #            By using those period and prescaler values, the timer will
    #            take over 35 minutes before overflow (thus virtually 
    #            eliminating any overflow errors). The prescaler, with a 
    #            frequency of 80 MHz, will allow for a count to equal 1
    #            microsecond, a convenient measurement.
    tim2 = pyb.Timer(2, period = 0x7fffffff, prescaler = 79)
    
    ## @brief    Channel 1 attribute for pyb timer for OC interrupt.
    #  @details  Channel 1 of Timer 2 will be used for the OC control. The 
    #            polarity of the A5 pin will be set to high.
    t2ch1 = tim2.channel(1, pyb.Timer.OC_TOGGLE, pin=pinA5, polarity=tim2.HIGH, callback=OCInterrupt)  

    ## @brief    Channel 2 attribute for pyb timer for IC interrupt.
    #  @details  Channel 2 of Timer 2 will be used for the IC control. The 
    #            polarity of the PB3 will be set to a falling edge (the 
    #            equivalent of the button being pressed instead of released).
    t2ch2 = tim2.channel(2, pyb.Timer.IC, pin=pinPB3, polarity=tim2.FALLING, callback=ICInterrupt)    
    
    ## @brief    Mainscript state 0 to wait for button to be pressed.      
    S0_M_WAITING    = 0
    ## @brief    Mainscript state 1 to calculate the reaction time.
    S1_M_CALCULATE  = 1   
    
    ## @brief    Variable to keep track if the button is pressed or not
    buttonPress  = False # initially button is not pressed
    
    ## @brief    List to hold the reaction times (later will be averaged)
    rxnTimes     = []
    
    ## @brief    Variable to display the average of the reaction times
    avgRxnTime    = 0 # The average starts at 0 since no reactions have been recorded
    
    ## @brief    Variable to determine the count at which the button was pressed.
    buttonPressCount = 0 # Start the count at 0
    
    ## @brief    lastCompareVal holds the previous compare value for OC
    #  @details  The lastCompareVal is essentially the 'start' to the new LEd
    #            control. If the LED is initially off, the lastCompareVal stores
    #            the time that the LED switched from on to off. If the LED is
    #            on, lastCompareVal stores the count at which the LED turned on.
    lastCompareVal = 0
    
    ## @brief    newCompareVal holds the future time to trigger the OC
    #  @details  The newCompareVal holds the count at which the OC interrupt
    #            will be triggered again. So, this value stores either the 
    #            random 2-3 seconds where the LED is off, or the exact 1 second
    #            where the LED is on for.
    if pinA5.value() == 0:
        print('LED initially off')
        newCompareVal = lastCompareVal + random.randrange(2000000,3000000)
        ## @brief    Mini finite state machine within the OC interrupt
        stateOC = 0
    else:
        print('LED initially on')
        newCompareVal = 10000
        stateOC = 1
    
    t2ch1.compare(newCompareVal) # set the channel to trigger on newCompareVal
    
    ## @brief    Main state machine state variable for mini FSM
    stateM       = 0 # Start the system in state 0
    
    # print intro message
    print('Welcome to the `Think Fast!` game! To play the game, start the '
          'Nucleo. As soon as the LED turns on, press the blue button as '
          'fast as possible. Do this for as long as you would like, and when '
          'finished, press Ctrl-C. This will display your average reaction '
          'time for the run. Hope you enjoy!')
    
    
    while True:
        try:
                
            if stateM == S0_M_WAITING:
                
                # If button is pressed, transition to the calculation state
                if buttonPress == True:
                    # print('Main: S0, button was pressed')
                    stateM = S1_M_CALCULATE
                    
                    ## @brief  Should the program calculate a reaction time?
                    calculate = True
                    buttonPress = False


            elif stateM == S1_M_CALCULATE:
                
                if calculate == True:
                    
                    ## @brief   The current reaction time for the run.
                    #  @details Calculate the reaction time in microseconds, which is 
                    #           convenient since the prescaler made the count equal to 1
                    #           microsecond.
                    
                    thisRxnTime = buttonPressCount - lastCompareVal
                    
                    if thisRxnTime > 0:
                        print('this reaction time was {:} microseconds ({:} seconds)'.format(thisRxnTime, thisRxnTime/1000000))
                        
                        # Add reaction time to the list.
                        rxnTimes.append(thisRxnTime)
                        
                        # Calculate the average reaction times of the current run.
                        avgRxnTime = sum(rxnTimes)/len(rxnTimes)
                        print('current average reaction time is now {:} microseconds ({:} seconds)\n'.format(avgRxnTime,avgRxnTime/1000000))
                
                        calculate = False   
                
                if pinA5.value() == 0:
                    # print('LED turned off')
                    # Transition back to waiting stage
                    stateM = S0_M_WAITING


        except KeyboardInterrupt: # press Ctrl-C to interupt 
            print('Ctrl-C has been pressed, exiting program')

            # show stats
            if avgRxnTime == 0:
                print('\n--- no reaction time was recorded ---\n')
            else:
                print('\n--- average reaction time for this session was {:} microseconds ({:} seconds) ---\n'.format(avgRxnTime,avgRxnTime/1000000))
            
            # Deinitialize the callback functions
            tim2.deinit()
            
            break
